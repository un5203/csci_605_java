/* 
 * StringBits.java
 *
 *  Version 1.01
 */



/**
 * 
 * This class converts a given character into an 8-bit string and vice-versa.
 * It also converts a 4 letter string into a 32-bit string and vice-versa.
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */


class StringBits {
	public static void main(String[] args) {
		StringBits str = new StringBits();
		//====Change the string here====
		String word = "woof";
		// =============================
		String wordToBits = str.strToByteString(word);
		String bitsToWord = str.byteStringToString(wordToBits);
		System.out.println("original string: "+word);
		System.out.println("bits: "+wordToBits);
		System.out.println("final string: "+bitsToWord);

	}
	public String charToByteString(char ch) {
		int temp = (int) ch;
		String str = String.valueOf((temp & 128) >> 7);
	    str += String.valueOf((temp & 64) >> 6);
	    str += String.valueOf((temp & 32) >> 5);
	    str += String.valueOf((temp & 16) >> 4);
	    str += String.valueOf((temp & 8) >> 3);
	    str += String.valueOf((temp & 4) >> 2);
	    str += String.valueOf((temp & 2) >> 1);
	    str += String.valueOf((temp & 1) >> 0);
    	return str;
	}
	public String strToByteString(String str) {
		String temp = charToByteString(str.charAt(0));
		temp += charToByteString(str.charAt(1));
		temp += charToByteString(str.charAt(2));
		temp += charToByteString(str.charAt(3));
		return temp;
	}

	public String byteStringToChar(String str) {
		int i = 64 * Integer.parseInt(String.valueOf(str.charAt(1)));
		i += 32 * Integer.parseInt(String.valueOf(str.charAt(2)));
		i += 16 * Integer.parseInt(String.valueOf(str.charAt(3)));
		i += 8 * Integer.parseInt(String.valueOf(str.charAt(4)));
		i += 4 * Integer.parseInt(String.valueOf(str.charAt(5)));
		i += 2 * Integer.parseInt(String.valueOf(str.charAt(6)));
		i += 1 * Integer.parseInt(String.valueOf(str.charAt(7)));
		return String.valueOf((char) i);
	}

	public String byteStringToString(String str) {
		String temp = byteStringToChar(str.substring(0,8));
		temp += byteStringToChar(str.substring(8,16));
		temp += byteStringToChar(str.substring(16,24));
		temp += byteStringToChar(str.substring(24,32));
		return temp;
	}
}