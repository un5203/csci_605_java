/* 
 * HelloJupiter.java
 *
 *  Version 1.01
 */

/**
 * 
 * This class displays a simple hello message 
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */


class HelloJupiter {
	/*
	 * This is the head of the main function
	 */
	public static void main(String[] args) {
		//This is the body of the main function
		hello();
		//Body of the main function ends here
	}

	/*
	 * This is the head of the hello function
	 */
	public static void hello() {
		//This is the body of the hello function
		System.out.println("Hello, Jupiter");
		//Body of the hello function ends here
	}
}