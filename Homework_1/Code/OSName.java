/* 
 * OSName.java
 *
 *  Version 1.01
 */

/**
 * 
 * This class displays the name of the operating system
 * from which this program is run.
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */


class OSName {
	public static void main(String[] args) {
		String osystem = System.getProperty("os.name");
		System.out.println(osystem);
	}
}