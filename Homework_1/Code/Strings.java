/* 
 * Strings.java
 *
 *  Version 1.01
 */

/**
 *
 * This class performs various string operations like concat, replace
 * substring, toLowerCase, toUpperCase and trim. The intention of this program
 * is to teach users about the immutability of strings.
 *
 * 
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */

public class Strings {
    public static void main(String args[]) {
        String original = "hello world";
        String temp = original;
        System.out.println("Original String"+"--> "+original);
        temp = original.concat("abc");
        System.out.println("Original String"+"--> "+original
        	+ " & after applying Concat function"+"--> "+temp);
        temp = original.replace("world","planet !");
        System.out.println("Original String"+"--> "+original
        	+" & after applying Replace function"+"--> "+temp);
        temp = original.substring(0,3);
        System.out.println("Original String"+"--> "+original
        	+" & after applying Substring function"+"--> "+temp);
        temp = original.toLowerCase();
        System.out.println("Original String"+"--> "+original
        	+" & after applying Lower Case function"+"--> "+temp);
        temp = original.toUpperCase();
        System.out.println("Original String"+"--> "+original
        	+" & after applying Upper Case function"+"--> "+temp);
        temp = original.trim();
        System.out.println("Original String"+"--> "+original
        	+" & after applying Trim function"+"--> "+temp);
    }
}
