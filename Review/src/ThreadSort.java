/*
 * ThreadSort.java
 *
 * Version 1.0.1
 */


import java.util.Random;
import java.util.Scanner;

/**
 * This class generates many threads as required and prints their id.
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */

public class ThreadSort implements Runnable {
    private int[] a;
    public ThreadSort(){
        Random rand = new Random();
        a = new int[1000];
        for (int i=0;i<1000;i++){
            a[i] = rand.nextInt(1000)+1;
        }
    }
    @Override
    public void run() {
        synchronized (this) {
            for (int i = 0; i < 1000; i++) {
                for (int j = i + 1; j < 1000; j++) {
                    if (a[i] > a[j]) {
                        int temp = a[i];
                        a[i] = a[j];
                        a[j] = temp;
                    }
                }
            }
            notifyAll();
        }

    }
    public void  printarray(){
        for(int i=0;i<1000;i++){
            System.out.print(a[i]+" ");
        }
    }
    public static void main(String[] args){
        try {
            ThreadSort ts = new ThreadSort();
            Scanner s = new Scanner(System.in);
            System.out.println("Enter the number of threads");
            int n = s.nextInt();
            long startTime = System.nanoTime();
            Thread[] threads = new Thread[n];
            for(int i=0;i<threads.length;i++){
                threads[i] = new Thread(ts);
                threads[i].start();
                threads[i].join();
            }
            long endTime = System.nanoTime();
            ts.printarray();
            System.out.println("\n\nTime taken to sort is "+ Long.toString((endTime - startTime)/1000000) + " milliseconds");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
