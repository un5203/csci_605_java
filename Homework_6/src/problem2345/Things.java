/**
 * Things.java is an generic interface for demonstrating LIFO and FIFO concepts
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */


package problem2345;

public interface Things<E> {

    /**
     * adds an item
     * @param item
     */
    public void push(E item);

    /**
     * removes and returns an item
     * @return
     */
    public E pop();

    /**
     * returns an item but doesn't remove it
     */
    public E peek();

    /**
     * returns the number of items in the collection
     */
    public int size();

    /**
     * returns true if collection is empty
     */
    public boolean isEmpty();

    /**
     * returns the contents of the collection as a string
     */
    public String toString();

}
