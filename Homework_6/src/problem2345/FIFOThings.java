/**
 * FIFOThings.java is a class that implements Queue collection technique (First In First Out)
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */

package problem2345;

public class FIFOThings<E> implements Things<E>{

    private E[] items;
    private int size = 0;


    /**
     * Constructor
     */
    public FIFOThings(){
        items = (E[]) new Object[10];
    }

    /**
     * adds an item
     *
     * @param item
     */
    @Override
    public void push(E item) {
        E[] temp = (E[]) new Object[items.length];
        //check to see if there is enough space to add new elements
        if(size()> items.length-5){
            for(int i = 0;i<items.length;i++){
                temp[i]= items[i];
            }
            items = (E[]) new Object[temp.length+30];
            for(int i = 0;i<temp.length;i++){
                items[i] = temp[i];
            }
        }

        //adding new elements
        for(int i = 0;i<items.length;i++){
            if(items[i] == null)
            {
                items[i] = item;
                size += 1;
                break;
            }
        }
    }

    /**
     * removes and returns an item
     *
     * @return
     */
    @Override
    public E pop() {
        if (items[0] != null) {
            E itemToPop = items[0];
            for(int i = 0;i<size();i++){
                items[i] = items[i+1];
            }
            items[size()-1] = null;
            size -= 1;
            return itemToPop;
        }
        else{
            return null;
        }
    }

    /**
     * returns an item but doesn't remove it
     */
    @Override
    public E peek() {
        return items[0];
    }

    /**
     * returns the number of items in the collection
     */
    @Override
    public int size() {
        return this.size;
    }

    /**
     * returns true if collection is empty
     */
    @Override
    public boolean isEmpty() {
        return (size()==0);
    }

    /**
     * returns a string of the items in the collection
     * @return
     */
    @Override
    public String toString(){
        String contents = "=> ";
        for(int i = size()-1;i> -1;i--){
            contents += items[i]+" ";
        }
        contents += "=>";
        return contents;
    }

    /**
     * main method that tests this class' implementations of generic Things methods
     * @param args
     */
    public static void main(String[] args) {
        Things<Integer> arrayOfInts = new FIFOThings<>();
        arrayOfInts.push(1);
        arrayOfInts.push(2);
        arrayOfInts.push(3);
        arrayOfInts.push(4);
        arrayOfInts.push(5);
        arrayOfInts.push(6);
        arrayOfInts.push(7);
        arrayOfInts.push(8);
        arrayOfInts.push(9);
        arrayOfInts.push(10);
        arrayOfInts.push(11);
        System.out.println(arrayOfInts.toString());
        arrayOfInts.pop();
        arrayOfInts.pop();
        arrayOfInts.pop();
        arrayOfInts.pop();
        System.out.println(arrayOfInts.toString());
    }
}
