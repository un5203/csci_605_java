/**
 * TestThings.java fully tests an implementation of Things.
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */

package problem2345;

public class TestThings {
    /**
     * tests the parameters for its sub functions
     * @param list - the collection of items to check
     * @param array - the array of items to be added to the list parameter
     * @param itemToAdd - the item to be added back to the list
     */
    public void testEverything(Things list,Object[] array,Object itemToAdd){
        for(int i = 0;i<array.length;i++){
            list.push(array[i]);
        }
        System.out.println("\n");
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("Original Contents of the List");
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println(list.toString());
        System.out.println("\n");
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("===== See which element can be removed next using peek() method");
        System.out.println(list.peek());
        System.out.println("\n");
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("===== After removing one item using pop() method");
        Object a = list.pop();
        System.out.println(list.toString());
        System.out.println("==> Item that was removed -->  "+ a);
        System.out.println("\n");
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("===== After adding the item "+ itemToAdd+ " using push() method");
        list.push(itemToAdd);
        System.out.println(list.toString());
    }

    //main function
    public static void main(String[] args) {
        TestThings test = new TestThings();
        Integer[] ints = {1,2,3,4,5};
        String[] strings = {"alpha","bravo","charlie","delta","echo"};
        Things<Integer> thingsOfInt = new LIFOThings<>();
        Things<String> thingsOfStr = new FIFOThings<>();
        System.out.println("~~~~~~~~~~~~ Testing FIFO List ~~~~~~~~~~~~");
        test.testEverything(thingsOfStr,strings,"foxtrot");
        System.out.println("~~~~~~~~~~~~ Testing LIFO List ~~~~~~~~~~~~");
        test.testEverything(thingsOfInt,ints,6);
    }
}
