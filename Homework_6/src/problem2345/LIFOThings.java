/**
 * LIFOThings.java is a class that implements Stack collection technique (Last In First Out)
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */

package problem2345;

public class LIFOThings<E> implements Things<E>{

    private E[] items;
    private int size = 0;


    /**
     * constructor
     */
    public LIFOThings() {
        items = (E[]) new Object[10];
    }

    /**
     * adds an item
     *
     * @param item
     */
    @Override
    public void push(E item) {
        E[] temp = (E[]) new Object[items.length];
        //check to see if there is enough space to add new elements
        if(size()> items.length-5){
            for(int i = 0;i<items.length;i++){
                temp[i]= items[i];
            }
            items = (E[]) new Object[temp.length+30];
            for(int i = 0;i<temp.length;i++){
                items[i] = temp[i];
            }
        }

        //adding new elements
        for(int i = 0;i<items.length;i++){
            if(items[i] == null)
            {
                items[i] = item;
                size += 1;
                break;
            }
        }
    }

    /**
     * removes and returns an item
     *
     * @return
     */
    @Override
    public E pop() {
        if(size()==0){
            System.out.println("No more elements left in the collection to pop !");
        }
        E[] temp = (E[]) new Object[size()+10]; //making a temp array to store items collection
        E itemToPop = items[size()-1]; //making a copy of the last item in the collection
        items[size-1] = null;
        for(int i=0;i<size();i++){
            temp[i] = items[i]; //copy items from main collection to a temp collection
        }
        size -= 1;
        items = temp;
        return itemToPop;
    }

    /**
     * returns an item but doesn't remove it
     */
    @Override
    public E peek() {
        return items[size()-1];
    }

    /**
     * returns the number of items in the collection
     */
    @Override
    public int size() {
        return this.size;
    }

    /**
     * returns true if collection is empty
     */
    @Override
    public boolean isEmpty() {
        return (size()==0);
    }


    @Override
    public String toString(){
        String contents = "";
        for(int i = size()-1;i> -1;i--){
            contents += "|| "+ items[i]+" ||" +"\n";
        }
        return contents;
    }

    /**
     * main function to test the above methods
     */
    public static void main(String[] args) {
        Things<Integer> arrayOfInts = new LIFOThings<>();
        arrayOfInts.push(1);
        arrayOfInts.push(2);
        arrayOfInts.push(3);
        arrayOfInts.push(4);
        arrayOfInts.push(5);
        arrayOfInts.push(6);
        arrayOfInts.push(7);
        arrayOfInts.push(8);
        arrayOfInts.push(9);
        arrayOfInts.push(10);
        arrayOfInts.push(11);
//        arrayOfInts.push(9);
//        arrayOfInts.push(10);
//        arrayOfInts.push(11);
        System.out.println(arrayOfInts.toString());
        arrayOfInts.pop();
        System.out.println(arrayOfInts.toString());
        arrayOfInts.push(20);
        System.out.println(arrayOfInts.toString());
//        System.out.println(arrayOfInts.peek());
//        System.out.println(arrayOfInts.pop());
//        System.out.println(arrayOfInts.peek());
//        System.out.println(arrayOfInts.pop());
//        System.out.println(arrayOfInts.peek());
//        System.out.println(arrayOfInts.pop());
//        System.out.println(arrayOfInts.peek());
//        System.out.println(arrayOfInts.pop());
//        System.out.println(arrayOfInts.peek());
//        System.out.println(arrayOfInts.pop());
//        System.out.println(arrayOfInts.peek());
//        System.out.println(arrayOfInts.pop());
//        System.out.println(arrayOfInts.isEmpty());
    }
}
