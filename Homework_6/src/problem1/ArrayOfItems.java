/*
 * ArrayOfItems.java
 *
 *  Version 1.01
 */

/**
 *
 * This class extends items
 *
 * @author  Ujwal Bharat
 * @author Yeshwanth Raja
 */
package problem1;
public class ArrayOfItems<L,R> extends Items<L,R>{

    /**
     * Variables that hold the items
     */
    private L[] leftItemList;
    private R[] rightItemList;
    private L[] leftTempHolder;
    private R[] rightTempHolder;

    /**
     * Intital constructor that creates a left and right item list and its temp holder
     */
    public ArrayOfItems() {
        leftItemList = (L[]) new Object[5];
        leftTempHolder = (L[]) new Object[5];
        rightItemList = (R[]) new Object[5];
        rightTempHolder = (R[]) new Object[5];
    }

    /**
     * Adds a pair of items to the array
     * @param l - item to be added to the left index
     * @param r - item to be added to the right index
     */
    @Override
    public void add(L l, R r) {
        if(size()>leftItemList.length-5){
            for(int i = 0;i<leftItemList.length;i++){
                leftTempHolder[i]= leftItemList[i];
                rightTempHolder[i]= rightItemList[i];
            }
            leftItemList = (L[]) new Object[leftTempHolder.length+30];
            rightItemList = (R[]) new Object[rightTempHolder.length+30];

            for(int i = 0;i<leftTempHolder.length;i++){
                leftItemList[i] = leftTempHolder[i];
                rightItemList[i] = rightTempHolder[i];
            }
        }
        for(int i = 0;i<leftItemList.length;i++){
            if(getLeft(i) == null)
            {
                leftItemList[i] = l;
                rightItemList[i] = r;
                break;
            }
        }
    }

    /**
     * returns the size of the collection
     * @return
     */
    @Override
    public int size() {
        if(leftItemList.length == rightItemList.length){
            int count = 0;
            for(int i=0;i<leftItemList.length;i++){
                if(getLeft(i)!= null){
                    count++;
                }
            }
            return count;
        }
        else{
            System.out.println("Item pair mismatch !");
            return 0;
        }
    }

    /**
     * function that returns an item from the left array using an index
     */
    /**
     * function that returns an item from the left array using an index
     * @param index
     * @return
     */
    @Override
    public L getLeft(int index) {
        return leftItemList[index];
    }


    /**
     * function that returns an item from the left array using an index
     * @param index
     * @return
     */
    @Override
    public R getRight(int index) {
        return rightItemList[index];
    }

    /**
     * function that returns a pair of items from the left and right array using an index
     * @param index
     * @return
     */
    @Override
    public Items get(int index) {
        Items<L,R> item = new ArrayOfItems<L, R>();
        item.add(leftItemList[index],rightItemList[index]);
        return item;
    }


    /**
     * returns the index of the first instance of the specific pair of items in the collection
     * @param l
     * @param r
     * @return
     */
    public int indexOf(L l, R r){
        for(int i = 0;i<leftItemList.length;i++){
            if(leftItemList[i]==l && rightItemList[i] == r){
                return i;
            }
        }
        return -1;
    }


    /**
     * removes the pair of items at the specified position in the collection
     * @param index
     */
    public void remove(int index){
        for(int i = index;i<leftItemList.length-1;i++){
            leftItemList[i] = leftItemList[i+1];
            rightItemList[i] = rightItemList[i+1];
        }
        leftItemList[leftItemList.length-1] = null;
        rightItemList[rightItemList.length-1] = null;

    }


    /**
     * main function that tests array of items
     * @param args
     */
    public static void main(String[] args) {
        Integer[] intArray = {0,1,2,3,4,5};
        String[] strArray = {"a","b","c","d","e","f"};
        Float[] fltArray = {2.0f,2.1f,2.2f,2.3f,2.4f,2.5f};
        Double[] dblArray = {3.0,3.1,3.2,3.3,3.4,3.5};
        String[] planets = {"Mercury","Venus","Earth","Mars","Jupiter","Saturn","Uranus","Neptune"};
        Integer[] removePlanetPosition = {1,2,3,4,5,6,7,8};
        String[] removePlanets = {"Mercury","Venus","Earth","Mars","Jupiter","Saturn","Uranus","Neptune"};
        Integer[] planetPosition = {1,2,3,4,5,6,7,8};
        Items<Integer,String> item = new ArrayOfItems<>();
        item.addAll(planetPosition,planets);
        System.out.println(item.indexOf(4,"Mars"));
        System.out.println(item.toString());
        item.removeAll(removePlanetPosition,removePlanets);
        System.out.println(item.toString());
    }
}