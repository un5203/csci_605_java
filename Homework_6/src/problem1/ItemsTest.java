
/*
 * ItemsTest.java
 *
 *  Version 1.01
 */

/**
 *
 * Tests the different implementations of the items collection
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */

package problem1;

public class ItemsTest {
    //function that accepts an array of items and tests its limits
    public static void testItems(Items items){
        Integer[] intArray = {0,1,2,3,4,5};
        String[] strArray = {"alpha","bravo","charlie","delta","echo","foxtrot"};
        Float[] fltArray = {2.0f,2.1f,2.2f,2.3f,2.4f,2.5f};
        Double[] dblArray = {3.0,3.1,3.2,3.3,3.4,3.5};
        items.addAll(intArray,strArray);
        System.out.println("========== Original List ==========="+"\n"+ items.toString());
        items.add(6,"georgia");
        System.out.println("========== After Adding a new item ==========="+"\n"+items.toString());
        items.remove(3);
        System.out.println("========== After Removing an item from position 3 ==========="+"\n"+items.toString());
        System.out.println("");
    }

    //main function that tests the ArrayOfItems and ListOfItems functionality
    public static void main(String[] args) {
        ItemsTest test = new ItemsTest();
        ListOfItems list = new ListOfItems();
        System.out.println("###############################");
        System.out.println("Using list");
        test.testItems(list);
        System.out.println("###############################");
        System.out.println("Using Array");
        ArrayOfItems array = new ArrayOfItems();
        test.testItems(array);
    }
}