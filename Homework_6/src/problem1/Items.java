/*
 * Items.java
 *
 *  Version 1.01
 */

/**
 *
 * This is an abstract class for items
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */

package problem1;
public abstract class Items<L,R>{

    //add one pair of left and right items to the collection
    public abstract void add(L l, R r);

    //adds all items in an array to the items collection
    public void addAll(L[] leftItems, R[] rightItems){
        for(int i = 0;i<leftItems.length;i++){
            add(leftItems[i],rightItems[i]);
        }
    };

    //returns the number of item pairs stored in the collection
    public abstract int size();

    //returns the item from the left of the collection at a particular index
    public abstract L getLeft(int index);

    //returns the item from the right of the collection at a particular index
    public abstract R getRight(int index);

    //returns the item pair from right and left of the collection at a particular index
    public abstract Items get(int index);

    //returns the index of the first instance of the specific pair of items in the collection
    public abstract int indexOf(L l, R r);

    //removes the pair of items at the specified position in the collection
    public abstract void remove(int position);


    /**
     * removes the specifed pair of items from the list
     * @param l item to be removed from left list
     * @param r item to be removed from right list
     */

    public void remove(L l, R r){
        int temp = indexOf(l,r);
        if(temp != -1) {
            remove(temp);
        }
    }

    /**
     * removes each pair of from the list if it is present
     * @param lefts array of left items to be removed
     * @param rights array of right items to be removed
     */
    public void removeAll(L[] lefts,R[] rights){
        int count = lefts.length;
        for(int i = 0;i<count;i++){
            remove(lefts[i],rights[i]);
        }
    }


    //prints the contents of the items array
    public String toString(){
        String output = "";
        for(int i = 0;i< size();i++){
            output += "\""+getLeft(i)+"\" : "+ "\""+ getRight(i)+"\"\n ";
        }
        return output;
    }

}