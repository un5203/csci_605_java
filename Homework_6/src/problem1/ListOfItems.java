/*
 * ListOfItems.java
 *
 *  Version 1.01
 */

/**
 * This class extends items and creates a linkedlist type
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */
package problem1;
public class ListOfItems<L, R> extends Items<L, R> {

    private Node head1;
    private Node tail1;
    private Node head2;
    private Node tail2;
    private int size = 0;
    private boolean firstTime = true; //checks if the add method is called for the first time or not

    public ListOfItems() {

        Node node1 = new Node();
        head1 = node1;
        tail1 = node1;

        Node node2 = new Node();
        head2 = node2;
        tail2 = node2;
    }

    //function that adds a pair of items to the array
    @Override
    public void add(L l, R r) {
        if (firstTime == true) {
            head1.setItem(l);
            head2.setItem(r);
            size += 1;
            firstTime = false;
        } else {
            Node node_left = new Node();
            node_left.setItem(l);

            tail1.setNext(node_left);
            tail1 = node_left;
            Node node_right = new Node();
            node_right.setItem(r);

            tail2.setNext(node_right);
            tail2 = node_right;
            size += 1;

        }
    }

    //function that returns a pair of items from the left and right array using an index
    @Override
    public Items get(int index) {

        Items<L,R> item = new ArrayOfItems<L, R>();
        item.add(getLeft(index),getRight(index));
        return item;

    }

    //function that returns the size of the array
    @Override
    public int size() {
        return this.size;
    }

    //function that returns an item from the left array using an index
    @Override
    public L getLeft(int index) {
        int count = 0;
        Node temp = head1;
        while (count < index) {
            if (temp.getNext() != null) {
                temp = temp.getNext();
                count++;
            } else {
                break;
            }
        }
        return (L) temp.getItem();
    }

    //function that returns an item from the right array using an index
    @Override
    public R getRight(int index) {
        int count = 0;
        Node temp = head2;
        while (count < index) {
            if (temp.getNext() != null) {
                temp = temp.getNext();
                count++;
            } else {
                break;
            }
        }
        return (R) temp.getItem();
    }

    @Override
    public int indexOf(L l,R r){
        Node temp1 = head1;
        Node temp2 = head2;
        int count = 0;
        while(count < size()){
            if(l == getLeft(count) && r == getRight(count)){
                return count;
            }
            else{
                temp1 = temp1.getNext();
                temp2 = temp2.getNext();
                count += 1;
            }
        }
        return -1;
    }

    //removes the pair of items at the specified position in the collection
    public void remove(int index){
        Node temp1 = head1;
        Node temp2 = head2;
        Node prev1 = head1,
             prev2 = head2;
        Node next1 = head1,
             next2 = head2;
        if(size()==1 && index == 0){
            head1 = null;
            tail1 = null;
            head2 = null;
            tail2 = null;
        }
        else if(index == 0){
            head1 = temp1.getNext();
            head2 = temp2.getNext();
        }
        else{
            for(int i = 0;i<index+1;i++){
                if(i < index-2){
                    temp1 = temp1.getNext();
                    temp2 = temp2.getNext();
                }
                else if(i == index-2){

                    prev1 = temp1.getNext();
                    temp1 = temp1.getNext();
                    prev2 = temp2.getNext();
                    temp2 = temp2.getNext();
                }
                else{
                        next1 = temp1.getNext();
                        next2 = temp2.getNext();
                        prev1.setNext(next1);
                        prev2.setNext(next2);
                        temp1 = prev1.getNext();
                        temp2 = prev2.getNext();
                }
            }
        }
        size -=1 ;
    }

    public static void main(String[] args) {
        String[] planets = {"Mercury","Venus","Earth","Mars","Jupiter","Saturn","Uranus","Neptune"};
        Integer[] planetPosition = {1,2,3,4,5,6,7,8};
        String[] a = {"a","b","c","d"};
        Integer[] b = {1,2,3,4};
        Items<Integer,String> item = new ListOfItems<>();
        //item.addAll(planetPosition,planets);
        item.addAll(b,a);
        item.addAll(planetPosition,planets);
        System.out.println(item.toString());
        System.out.println(item.indexOf(5,"Jupiter"));
        System.out.println(item.indexOf(6,"Saturn"));
        item.removeAll(b,a);
        System.out.println(item.toString());
        item.removeAll(planetPosition,planets);
        System.out.println(item.toString());
    }
}
