package problem1;/*
 * Node.java
 *
 *  Version 1.01
 */

/**
 *
 * This class creates a node object
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */

public class Node {
    private Object item;
    private Node next; //reference to another node


    //setters and getters
    public Object getItem() {
        return item;
    }

    public void setItem(Object item) {
        this.item = item;
    }

    public Node getNext() {
        return this.next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

}
