/*
 * Items.java
 *
 *  Version 1.01
 */

/**
 *
 * This is an abstract class for items
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */

public abstract class Items {

    //add one item to the collection
    public abstract void add(Object item);


    //adds all items in an array to the items collection
    public void addAll(Object[] items){
        for(int i = 0;i<items.length;i++){
            add(items[i]);
        }
    };

    //returns an item at a specified index
    public abstract Object get(int index);

    //returns the number of items stored in the collection
    public abstract int size();


    public String toString(){
        String output = "";

        for(int i = 0;i< size();i++){
            output += get(i) +" ";
            System.out.print(get(i)+" ");
        }
        return output;
    }
}
