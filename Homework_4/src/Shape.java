/*
 * Shape.java
 *
 *  Version 1.01
 */

/**
 *
 * This class is an interface to create different shapes
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */

public interface Shape {
    public String getName();
    public double area();
    public void draw();
}
