/*
 * ShapePrinter.java
 *
 *  Version 1.01
 */

/**
 *
 * This class prints shapes
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */



import java.util.Objects;
import java.util.Scanner;


public class ShapePrinter {

    //this method will print the name, area and draw the shape
    public void printShape(Shape shape) {
        System.out.println(shape.getName());
        System.out.println("Area is" + " " + shape.area());
        shape.draw();
    }


    //main method
    public static void main(String args[]) {
        String shapename;
        ShapePrinter sp = new ShapePrinter();
        Scanner s = new Scanner(System.in);
        System.out.println("Enter the type of shape u want(Rectangle,Square,Circle,Triangle otherwise quit");
        shapename = s.next();
        if(!shapename.equals("quit")) {
            if (shapename.equalsIgnoreCase("Rectangle")) {
                System.out.println("Enter Length and Breadth");
                int length = s.nextInt();
                int breadth = s.nextInt();
                Rectangle rec = new Rectangle(shapename, length, breadth);
                sp.printShape(rec);
            } else if (shapename.equalsIgnoreCase("Triangle")) {
                System.out.println("Enter Base and Height");
                int base = s.nextInt();
                int height = s.nextInt();
                Triangle tri = new Triangle(shapename, base, height);
                sp.printShape(tri);
            } else if (shapename.equalsIgnoreCase("Circle")) {
                System.out.println("Enter Radius");
                int radius = s.nextInt();
                Circle circle = new Circle(shapename,radius);
                sp.printShape(circle);
            } else if (shapename.equalsIgnoreCase("Square")) {
                System.out.println("Enter Length of side of square");
                int length = s.nextInt();
                Square sq = new Square(shapename, length, length);
                sp.printShape(sq);
            }
        }
    }
}
