/*
 * ListOfItems.java
 *
 *  Version 1.01
 */

/**
 *
 * This class extends items and creates a linkedlist type
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */

public class ListOfItems extends Items{

    private Node head;
    private Node tail;
    private int size = 0;
    private boolean firstTime=true; //checks if the add method is called for the first time or not

    public ListOfItems(){
        Node node = new Node();
        head = node;
        tail = node;
    }

    @Override
    public void add(Object item) {
        if(firstTime==true){
            head.setItem(item);
            size += 1;
            firstTime = false;
        }
        else{
            Node node = new Node();
            node.setItem(item);
            tail.setNext(node);
            tail = node;
            size += 1;
        }
    }

    @Override
    public Object get(int index) {
        int count = 0;
        Node temp = head;
        while(count < index){
            if(temp.getNext() != null){
                temp = temp.getNext();
                count++;
            }
            else{
                break;
            }
        }
        return temp.getItem();
    }

    @Override
    public int size() {
        return this.size;
    }

}
