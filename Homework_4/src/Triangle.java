/*
 * Trianlge.java
 *
 *  Version 1.01
 */

/**
 *
 * This class creates a triangle object that implements shape
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */


import java.util.Scanner;

public class Triangle implements Shape{
    private String shapename;
    private int base;
    private int height;

    //constructor
    public Triangle(String shapename, int base, int height) {
        this.shapename = shapename;
        this.base = base;
        this.height = height;
    }

    //gets the name of the shape
    @Override
    public String getName() {
        return shapename;
    }

    //gets the area of the triangle
    @Override
    public double area() {
        return 0.5*base*height;
    }


    //draws the triangle
    @Override
    public void draw() {
        for(int i=0;i<height;i++){
            for(int j=0;j<=i;j++){
                System.out.print("*");
            }
            System.out.println();
        }

    }

    //main function
    public static void main(String args[]) {
        int base,height;
        Scanner s = new Scanner(System.in);
        System.out.println("Enter the Base and Height of Triangle");
        base = s.nextInt();
        height = s.nextInt();
        Triangle tri = new Triangle("Triangle",base,height);
        tri.draw();
    }
}
