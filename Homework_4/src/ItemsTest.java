/*
 * ItemsTest.java
 *
 *  Version 1.01
 */

/**
 *
 * Tests the differnet implementations of the items
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */

public class ItemsTest {
    public static void testItems(Items items){
        String[] str= {"apple", "banana", "cashew"};
        items.addAll(str);
        System.out.println("Original List ----"+" "+ items.toString());
        items.add("oranges");
        System.out.println("After Adding new item -----------"+" "+items.toString());
        System.out.println("Size is "+" "+items.size());
        System.out.println("Getting Item at 0 position --"+" "+items.get(0));
        System.out.println("");

    }
    public static void main(String[] args) {
        ItemsTest test = new ItemsTest();
        ListOfItems list = new ListOfItems();
        System.out.println("Using list");
        test.testItems(list);
        ArrayOfItems array = new ArrayOfItems(5);
        System.out.println("Using Array");
        test.testItems(array);
    }
}