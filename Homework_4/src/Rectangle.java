/*
 * Rectangle.java
 *
 *  Version 1.01
 */

/**
 *
 * This class creates a rectangle object that implements shape
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */


import java.util.Scanner;

public class Rectangle implements Shape {
    private String shapename;
    private int length;
    private int width;

    //constructor
    public Rectangle(String shapename,int length,int width){
        this.shapename=shapename;
        this.length=length;
        this.width=width;
    }

    //the following functions implement the contract from shape interface.
    @Override
    public String getName() {
        return shapename;
    }

    //this methods calculate the area of the rectangle
    @Override
    public double area() {
        return length*width;
    }

    //this method draws the rectangle
    @Override
    public void draw() {
        for (int i=0;i<length;i++){
            for(int j=0;j<width;j++){
                System.out.print("*");
            }
            System.out.println();
        }

    }

    //this is the main function
    public static void main(String args[]) {
        int length,width;
        Scanner s = new Scanner(System.in);
        System.out.println("Enter the Length and width of Rectangle");
        length = s.nextInt();
        width = s.nextInt();
        Rectangle rec = new Rectangle("Rectangle",length,width);
        rec.draw();
    }
}
