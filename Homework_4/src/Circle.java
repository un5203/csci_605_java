/*
 * Circle.java
 *
 *  Version 1.01
 */

/**
 *
 * This class creates a circle object that implements shape
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */


import java.util.Scanner;

public class Circle implements Shape {
    private int radius;
    private String shapename;

    //constructor
    public Circle(String shapename,int radius){
        this.shapename = shapename;
        this.radius = radius;
    }


    //gets the name of the shape
    @Override
    public String getName() {
        return shapename;
    }

    //gets the area of the circle
    @Override
    public double area() {
        return 3.14*radius*radius;
    }

    //draws the circle
    @Override
    public void draw() {
        System.out.println("    *  *    ");
        System.out.println(" *        * ");
        System.out.println("*          *");
        System.out.println("*          *");
        System.out.println(" *        * ");
        System.out.println("    *  *    ");

    }

    //main method
    public static void main(String args[]) {
        int radius;
        Scanner s = new Scanner(System.in);
        System.out.println("Enter the Radius");
        radius = s.nextInt();
        Circle circ = new Circle("Circle",radius);
        circ.draw();
    }
}
