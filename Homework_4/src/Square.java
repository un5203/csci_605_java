/*
 * Square.java
 *
 *  Version 1.01
 */

/**
 *
 * This class creates a square object that extends rectangle
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */


import java.util.Scanner;

public class Square extends Rectangle {

    //constructor
    public Square(String shapename, int length, int width) {
        super(shapename, length, width);
    }
    //main method
    public static void main(String args[]){
        Scanner s = new Scanner(System.in);
        System.out.println("Enter Length of Side of the Square");
        int length = s.nextInt();
        Square sq = new Square("Square",length,length);
        sq.draw();
    }
}
