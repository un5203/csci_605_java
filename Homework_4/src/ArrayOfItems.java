/*
 * ArrayOfItems.java
 *
 *  Version 1.01
 */

/**
 *
 * This class extends items
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */


public class ArrayOfItems extends Items{
    private Object[] itemsHolder;
    private Object[] tempHolder;
    private int capacity; //denotes the full capacity of the array

    //constructor
    public ArrayOfItems(int initialSize){
        itemsHolder = new Object[initialSize];
        tempHolder = new Object[initialSize];
    }


    //adds a new item to array
    @Override
    public void add(Object item) {
        if(size()> itemsHolder.length-5){
            for(int i = 0;i<itemsHolder.length;i++){
                tempHolder[i]= itemsHolder[i];
            }
            itemsHolder = new Object[tempHolder.length+30];
            capacity = itemsHolder.length;
            for(int i = 0;i<tempHolder.length;i++){
                itemsHolder[i] = tempHolder[i];
            }
        }
        for(int i = 0;i<itemsHolder.length;i++){
            if(get(i) == null)
            {
                itemsHolder[i] = item;
                break;
            }
        }
    }

    //returns the item at an index
    @Override
    public Object get(int index) {
        return itemsHolder[index];
    }

    //returns the true size of the array
    @Override
    public int size() {
        int count = 0;
        for(int i=0;i<itemsHolder.length;i++){
            if(get(i)!= null){
                count++;
            }
        }
        return count;
    }

    //main method
    public static void main(String[] args) {
        String[] str= {"a","b","c","d","e","f"};
        ArrayOfItems collection = new ArrayOfItems(5);
        collection.addAll(str);
        collection.add("hello");
        String[] str2 = {"g","h","i","j","k","l","m","n","o","p"};
        collection.toString();
        System.out.println("Used "+collection.size()+" out of "+ collection.capacity);
        collection.addAll(str2);
        collection.toString();
        System.out.println("Used "+collection.size()+" out of "+ collection.capacity);
        //System.out.println(collection.size());
    }
}