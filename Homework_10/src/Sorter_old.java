/*
 * Sorter.java
 *
 * Version 1.0.1
 */

import java.util.Random;
import java.util.Scanner;

/**
 * This class generates many threads as required and uses them to bubble sort an array.
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */
public class Sorter_old implements Runnable{
    private static final int elements = 1000;
    private static int[] mainArray = new int[elements];
    private boolean sorted = false;
    private static int threadCount;
    private static boolean[] runAgain;
    private static final Object lock = new Object();
    private static boolean printedOnce = false;
    private long startTime;
    private static long endTime = 0;

    /**
     * Constructor that generates a randomized array
     */
    public Sorter_old(){
        Random rand = new Random();
        for(int i = 0;i<elements;i++){
            mainArray[i] = rand.nextInt(elements) + 1;
        }
    }

    /**
     * Sorts a part of an array using bubble sort algorithm
     * @param array
     * @param initialPos
     * @param finalPos
     */
    public void bubbleSorter(int[] array,int initialPos, int finalPos){
        boolean swap = true;
        while(swap){
            swap = false;
            for(int i = initialPos;i< finalPos ;i++){
                if(array[i] > array[i+1]){
                    int temp = array[i];
                    array[i] = array[i+1];
                    array[i+1] = temp;
                    swap = true;
                }
            }
        }
    }

    /**
     * Sorts the complete array using bubble sort
     * @param todo
     */
    public void bubbleSorter(int[] todo){
        boolean swap = true;
        while(swap){
            swap = false;
            for(int i = 0;i< todo.length - 1;i++){
                if(todo[i] > todo[i+1]){
                    int temp = todo[i];
                    todo[i] = todo[i+1];
                    todo[i+1] = temp;
                    swap = true;
                }
            }
        }
    }

    /**
     * Main function that performs bubble sort using multiple threads.
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        Sorter_old sorter = new Sorter_old();
        printArray(sorter.mainArray);
        System.out.println("\n");
        System.out.println("Enter the number of threads:\n");
        Scanner input = new Scanner(System.in);
        threadCount = input.nextInt();
        Thread[] threads = new Thread[threadCount+1];
        System.out.println(Thread.activeCount());
        runAgain = new boolean[threadCount+1];
        for(int i = 1;i<runAgain.length;i++){
            runAgain[i] = true;
        }
        sorter.startTime = System.nanoTime();
        for (int i = 1; i < threads.length;i++){
            threads[i] = new Thread(sorter);
            threads[i].setName(Integer.toString(i));
            threads[i].start();
        }
    }

    /**
     * prints a given array
     * @param array
     */
    public static void printArray(int[] array) {
        for(int i = 0 ;i< elements;i++){
            System.out.print(array[i] + " ");
        }
    }

    /**
     * checks if the array is sorted
     * @return
     */
    public boolean sorted() {
        for (int i = 0; i < mainArray.length - 1; i++) {
            if (mainArray[i] > mainArray[i + 1]) {
                return false;
            }
        }
        return true;
    }

    /**
     * function that performs the actual sort
     * @param lock
     * @throws InterruptedException
     */
    public void sorting(Object lock) throws InterruptedException {
        if(threadCount>1){
            int id = Integer.parseInt(Thread.currentThread().getName());
            int finalPos = (id * (elements/threadCount)) - 1;
            int initialPos = (id == 1) ? 0: ((id-1) * (elements/threadCount)) - 1;
            int initialValue = mainArray[initialPos];
            int finalValue = mainArray[finalPos];
            bubbleSorter(mainArray,initialPos,finalPos);
            if(initialValue != mainArray[initialPos]){
                if( (id-1) != 0 ){
                    runAgain[id-1]= true;
                }
                else{
                    runAgain[id] = true;
                }
            }
            else{
                if( (id-1) != 0 ){
                    runAgain[id-1]= false;
                }
                else{
                    runAgain[id] = false;
                }
            }
            if(finalValue != mainArray[finalPos]){
                if( id < threadCount ){
                    runAgain[id+1]= true;
                }
                else{
                    runAgain[id] = true;
                }
            }
            else{
                if( id < threadCount ){
                    runAgain[id+1]= false;
                }
                else{
                    runAgain[id] = false;
                }
            }
            lock.notifyAll();
            lock.wait();
            lock.notifyAll();
        }
    }
    
    
    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        if(threadCount==1){
            bubbleSorter(mainArray);
            endTime = System.nanoTime();
            printArray(mainArray);
            System.out.println("Time taken to sort is "+ ((endTime-startTime)/1000000) + " milliseconds");
        }
        else{
            while(!sorted) {
                synchronized (lock){
                    try {
                        int id = Integer.parseInt(Thread.currentThread().getName());
                        sorting(lock);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                sorted = sorted();
                if(endTime == 0 && sorted){
                    endTime = System.nanoTime();
                }
                if(Integer.parseInt(Thread.currentThread().getName())==1 && sorted){
                    printArray(mainArray);
                    System.out.println("Time taken to sort is "+ ((endTime-startTime)/1000000) + " milliseconds");
                }
            }
        }


    }
}
