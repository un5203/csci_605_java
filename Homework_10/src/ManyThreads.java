/*
 * ManyThreads.java
 *
 * Version 1.0.1
 */

import java.util.Scanner;

/**
 * This class generates many threads as required and prints their id.
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */

public class ManyThreads implements Runnable {

    private int number; //number of threads to be created
    private static final Object lock = new Object(); //lock to synchronize on
    private int id = 1; //starting thread id

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        while (true) {
             synchronized (lock)  {
                if(Thread.currentThread().getName().equals(Integer.toString(id))){
                    System.out.print(Thread.currentThread().getName());
                    if (id == number){
                        id = 1;
                        try {
                            lock.notifyAll();
                            Thread.sleep(800);
                            lock.wait();
                            lock.notifyAll();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    else{
                        id ++;
                        try {
                            lock.notifyAll();
                            lock.wait();
                            lock.notifyAll();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else{
                    try {
                        lock.notifyAll();
                        lock.wait();
                        lock.notifyAll();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }

        }
    }

    /**
     * Main function that generates many threads as required and prints their id.
     *
     * @param args
     */
    public static void main(String[] args) throws InterruptedException {
        ManyThreads idPrinter = new ManyThreads();
        System.out.println("Enter any number greater than or equal to 2:\n");
        Scanner input = new Scanner(System.in);
        idPrinter.number = input.nextInt();
        Thread[] threads = new Thread[idPrinter.number];
        for ( int i = 1; i <= threads.length ; i++ ){
            Thread thread = new Thread(idPrinter);
            thread.setName(Integer.toString(i));
            thread.start();
        }
    }
}
