/*
 * Sorter.java
 *
 * Version 1.0.1
 */

import java.util.Random;
import java.util.Scanner;

/**
 * This class generates many threads as required and uses them to bubble sort an array.
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */
public class Sorter implements Runnable{

    private static int elements = 1000;
    private static final int[] array = new int[elements];

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {

    }


    public Sorter(){
        Random rand = new Random();
        for(int i = 0;i<elements;i++){
            array[i] = rand.nextInt(elements) + 1;
        }
    }

    /**
     * Sorts a part of an array using bubble sort algorithm
     * @param array
     * @param initialPos
     * @param finalPos
     */
    public void bubbleSorter(int[] array,int initialPos, int finalPos){
        boolean swap = true;
        while(swap){
            swap = false;
            for(int i = initialPos;i< finalPos ;i++){
                if(array[i] > array[i+1]){
                    int temp = array[i];
                    array[i] = array[i+1];
                    array[i+1] = temp;
                    swap = true;
                }
            }
        }
    }

    /**
     * Sorts the complete array using bubble sort
     * @param todo
     */
    public void bubbleSorter(int[] todo){
        boolean swap = true;
        while(swap){
            swap = false;
            for(int i = 0;i< todo.length - 1;i++){
                if(todo[i] > todo[i+1]){
                    int temp = todo[i];
                    todo[i] = todo[i+1];
                    todo[i+1] = temp;
                    swap = true;
                }
            }
        }
    }


    public void printArray(int[] array){
        for(int i = 0 ; i < array.length; i++){
            System.out.print(array[i]+" ");
        }
    }

    public static void main(String[] args) {
        Sorter sorter = new Sorter();
        sorter.printArray(array);
    }
}
