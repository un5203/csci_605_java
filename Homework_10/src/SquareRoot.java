/*
 * SquareRoot.java
 *
 * Version 1.0.1
 */

import com.sun.tools.doclets.internal.toolkit.util.DocFinder;
import com.sun.xml.internal.ws.api.message.ExceptionHasMessage;

import java.util.Scanner;

/**
 * This class calculates square roots of numbers using threading.
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */

public class SquareRoot implements Runnable{
    private boolean done = false;
    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        while (!done) {
            try {
                System.out.println("Enter a number:");
                Scanner input = new Scanner(System.in);
                float number = input.nextFloat();
                if(number < 0){
                    System.out.println("Exiting...");
                    done = true;
                }
                else{
                    System.out.println("\nThe square root of " + number + " is " + Math.sqrt(number)+"\n");
                }
            } catch (Exception e) {
                System.out.println("\n !!! You have entered an invalid option ! Try again !!!!\n");
            }
        }
    }

    /**
     * Main method that starts the thread and waits for it to complete.
     * @param args
     */
    public static void main(String[] args) {
        Thread thread = new Thread(new SquareRoot());
        thread.start();
    }
}
