/*
 * Counter.java
 *
 * Version 1.0.1
 */

import java.util.Scanner;

/**
 * This class counts till a given number in two separate threads.
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */

public class Counter implements Runnable {

    private int count = 1;
    private boolean firstTime = true;
    private int number = 2;
    private static final Object lock = new Object();

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        synchronized (lock){
            while(count<=number){
                if(firstTime && !Thread.currentThread().getName().equals("thread_one")){
                    try {
                        lock.wait();
                        lock.notify();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println(Thread.currentThread().getName()+": "+count);
                count++;
                lock.notify();
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                lock.notify();
                firstTime = false;
            }
        }
    }

    /**
     * Main method that counts using two threads.
     * @param args
     */
    public static void main(String[] args) throws InterruptedException {
        Counter counter = new Counter();
        System.out.println("Enter a number to count:\n");
        Scanner input = new Scanner(System.in);
        counter.number = input.nextInt();
        Thread thread_one = new Thread(counter);
        Thread thread_two = new Thread(counter);
        Thread thread_three = new Thread(counter);
        Thread thread_four = new Thread(counter);
        thread_one.setName("thread_one");
        thread_two.setName("thread_two");
        thread_two.start();
        thread_one.start();
    }
}
