public class MergeSort implements SortingAlgorithm {
    private double timeTaken;
    private int count;
    int[] array_copy;
    int[] temp;

    @Override
    public String getName() {
        /**
         * returns the name of the sorting algorithm
         *
         * @return String
         */
        return "Merge Sort";
    }

    @Override
    public int[] sortArray(int[] array) {
        /**
         * Takes an int array as a parameter and sorts it.
         *
         * @param array - array to be sorted
         * @return int[] - returns sorted int array
         */
        array_copy = array;
        temp = new int[array.length];
        double startTime = System.nanoTime();
        int sorted[] = merge_sort(0, array.length - 1);
        double stopTime = System.nanoTime();
        timeTaken = (stopTime - startTime);
        return sorted;
    }

    private int[] merge_sort(int low, int high) {
        /**
         * Divides the array into two and sorts each part
         * @param low - lower index
         * @param high - last index
         * @return int[] - sorted array after merging all the parts together
         */

        int[] sorted_array = new int[10];
        if (low < high) {
            count += 1;
            // Get the middle element
            int mid = (low + high) / 2;
            // Sort left side
            merge_sort(low, mid);
            // Sort Right Side
            merge_sort(mid + 1, high);
            // Combine both sides
            sorted_array = merge_parts(low, mid, high);
        }
        return sorted_array;
    }

    private int[] merge_parts(int low, int mid, int high) {
        /**
         * Merges the sorted parts into one array
         * @param low - lower index
         * @param mid - middle index
         * @param high - last index
         * @return int[] - sorted array of each part merged into one array
         */

        // Combine both parts into a temporary array
        System.arraycopy(array_copy, low, temp, low, high + 1 - low);
        int i = low;
        int j = mid + 1;
        int k = low;
        // Copy the smallest values from the left or right side to the original array copy
        while ((i <= mid) && (j <= high)) {
            count += 1;
            if (temp[i] < temp[j]) {
                array_copy[k] = temp[i];
                i++;
                k++;
            } else {
                array_copy[k] = temp[j];
                j++;
                k++;
            }
        }
        // Copy the remaining elements
        while (i <= mid) {
            array_copy[k] = temp[i];
            i++;
            k++;
        }
        return array_copy;

    }


    @Override
    public double timeTakenToSort() {
        /**
         * Calculates the time taken to sort an array in milliseconds
         *
         * @return double: the time taken to sort an array in milliseconds
         */
        return timeTaken / 1000000;
    }

    @Override
    public int numberOfOperations() {
        /**
         * Calculates the number of operations (copy/swap/compare) performed by the sorting algorithm
         *
         * @return int: number of operations.
         */
        return count;
    }
}
