
public class QuickSort implements SortingAlgorithm {
    private double timeTaken;
    private int count;
    private int[] array_copy;

    @Override
    public String getName() {
        /**
         * returns the name of the sorting algorithm
         *
         * @return String
         */
        return "Quick Sort";
    }

    @Override
    public int[] sortArray(int[] array) {
        /**
         * Takes an int array as a parameter and sorts it.
         *
         * @param array - array to be sorted
         * @return int[] - returns sorted int array
         */
        array_copy = array;
        double startTime = System.nanoTime();
        int sorted_array[] = quick_sort(0, array.length - 1);
        double stopTime = System.nanoTime();
        timeTaken = (stopTime - startTime);
        return sorted_array;
    }

    private int[] quick_sort(int low, int high) {
        /**
         * Divides the array into two based on the pivot and sorts each part
         * @param low - lower index
         * @param high - last index
         * @return int[] - sorted array
         */
        int i = low;
        int j = high;
        // Taking pivot as the last element
        int pivot = array_copy[j];
        // Dividing into 2 arrays
        while (i <= j) {
            while (array_copy[i] < pivot) {
                count += 1;
                i++;
            }
            while (array_copy[j] > pivot) {
                count += 1;
                j--;
            }
            if (i <= j) {
                count += 1;
                int temp = array_copy[i];
                array_copy[i] = array_copy[j];
                array_copy[j] = temp;

                // Moving index to next position on both sides
                i++;
                j--;
            }
        }
        if (low < j)
            quick_sort(low, j);
        if (i < high)
            quick_sort(i, high);
        return array_copy;
    }


    @Override
    public double timeTakenToSort() {
        /**
         * Calculates the time taken to sort an array in milliseconds
         *
         * @return double: the time taken to sort an array in milliseconds
         */
        return timeTaken / 1000000;
    }

    @Override
    public int numberOfOperations() {
        /**
         * Calculates the number of operations (copy/swap/compare) performed by the sorting algorithm
         *
         * @return int: number of operations.
         */
        return count;
    }

}
