/*
 * Algorithms.java
 *
 *  Version 1.01
 */

import com.sun.tools.doclets.formats.html.SourceToHTMLConverter;

import java.util.Random;

/**
 * This class implements the methods from SortingAlgorithm interface.
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */

public class SortTester<Algorithm extends SortingAlgorithm> {

    private Algorithm algorithm;
    private double averageTime = 0;

    public void setSortingAlgorithm(Algorithm algorithm) {
        /**
         * Sets the type of sorting algorithm based on the generic Type Algorithm
         * @param Algorithm - Generic type that accepts either Merge,Quick or Bubble Sort
         */
        this.algorithm = algorithm;
    }

    public int[] passArray(int[] array) {
        /**
         * Performs the sorting based on the type of algorithm set
         * @param array - array to be sorted
         * @return int[] - returns the sorted array
         */
        return algorithm.sortArray(array);
    }

    private int[] generateSortedArray(int arraySize) {
        /**
         * Generates a pre Sorted array
         * @param arraysize - Size of the array
         * @return int[] - returns a pre sorted array
         */
        int[] array = new int[arraySize];
        int j=0;
        for(int i=1;i<=arraySize;i++){
            array[j]=i;
            j++;
        }
        return array;
    }
    private int[] generateReverseArray(int arraySize) {
        /**
         * Generates an array in Reverse order
         * @param arraysize - Size of the array
         * @return int[] - returns a reverse array
         */
        int[] array = new int[arraySize];
        int j=0;
        for(int i=arraySize;i>0;i--){
            array[j]=i;
            j++;
        }
        return array;
    }
    private int[] generateRandomArray(int arraySize) {
        /**
         * Generates a Random array
         * @param arraysize - Size of the array
         * @return int[] - Returns a array with random integers from 1-10
         */
        int[] array = new int[arraySize];
        Random rand = new Random();
        for(int i=0;i<arraySize;i++){
            array[i] = rand.nextInt(10)+1;
        }
        return array;
    }


    public void testAlgorithm(int arraySize) {
        /**
         * Tests the algorithm first with a pre sorted array, Reverse sorted array and a random array generated 100 times
         * @param arraySize - size of the array
         */
        int[] pre_sorted = generateSortedArray(arraySize);
        for(int i = 0;i<pre_sorted.length;i++){
            System.out.println(pre_sorted[i]);
        }
        System.out.println("******");
        int [] reverse_sorted = generateReverseArray(arraySize);
        for(int i = 0;i<reverse_sorted.length;i++){
            System.out.println(reverse_sorted[i]);
        }
        System.out.println("******");
        int [] random = new int[arraySize];

        // Sorts the pre sorted array
        pre_sorted = passArray(pre_sorted);
        // Sorts the Reverse Sorted array
        reverse_sorted = passArray(reverse_sorted);
        // Generates a random array and tests it 100 times
        for(int i =0;i<100;i++){
            int[] random_array = generateRandomArray(arraySize);
            random = passArray(random_array);
            averageTime += algorithm.timeTakenToSort();
        }

        System.out.print("[ ");
        for (int i=0;i<pre_sorted.length;i++) {
            System.out.print(pre_sorted[i]);
            if (i != pre_sorted[pre_sorted.length - 1]) {
                System.out.print(" ");
            }
        }
        System.out.println("] -----> Pre_Sorted");

        System.out.print("[ ");
        for (int i=0;i<reverse_sorted.length;i++) {
            System.out.print(reverse_sorted[i]);
            if (i != reverse_sorted[reverse_sorted.length - 1]) {
                System.out.print(" ");
            }
        }
        System.out.println("] -----> Reverse_Array_Sorted");

        System.out.print("[ ");
        for (int i=0;i<random.length;i++) {
            System.out.print(random[i]);
            if (i != random[random.length-1]) {
                System.out.print(" ");
            }
        }
        System.out.println("] ------> Random_Array_Sorted");

        System.out.println(algorithm.getName());
        System.out.println("This test runs three different kinds of sorting algorithms, namely Bubblesort, Mergesort " +
                "and Quicksort. \n This test also calculates the average time taken by each alogrithm to sort different" +
                " kinds of arrays  ");
        System.out.println("The number of Comparisons" + " " + algorithm.numberOfOperations());
        System.out.println("The time taken for one sort: " + " " + algorithm.timeTakenToSort());
        System.out.println("The average time taken for 100 sorting operations of randomly generated array: "+ averageTime/100);
    }

    /**
     * This is the main function
     * @param args
     */
    public static void main(String[] args) {
        QuickSort quicksort = new QuickSort();
        MergeSort mergesort = new MergeSort();
        BubbleSort bubblesort = new BubbleSort();
        SortTester test = new SortTester();
        test.setSortingAlgorithm(bubblesort);
        test.testAlgorithm(5);
        test.setSortingAlgorithm(mergesort);
        test.testAlgorithm(5);
        test.setSortingAlgorithm(quicksort);
        test.testAlgorithm(5);
    }

}
