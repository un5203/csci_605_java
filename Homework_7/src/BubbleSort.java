public class BubbleSort implements SortingAlgorithm{

    private double timeTaken;
    private int count;

    /**
     * returns the name of the sorting algorithm
     *
     * @return String
     */
    @Override
    public String getName() {
        return "Bubble Sort";
    }

    /**
     * Takes an int array as a parameter and sorts it.
     *
     * @param array - array to be sorted
     * @return int[] - returns sorted int array
     */
    @Override
    public int[] sortArray(int[] array) {
        double startTime = System.nanoTime();
        boolean swapped = false;
        do {
            swapped = false;
            for( int i=0; i<array.length-1; i++ ) {
                count +=1 ;
                if(array[i] > array[i+1]) {
                    int tmp = array[i];
                    array[i] = array[i+1];
                    array[i+1] = tmp;
                    swapped = true;
                }
            }
        } while(swapped);
        double stopTime = System.nanoTime();
        timeTaken = (stopTime-startTime);
        return array;
    }

    /**
     * Calculates the time taken to sort an array in milliseconds
     *
     * @return double: the time taken to sort an array in milliseconds
     */
    @Override
    public double timeTakenToSort() {
        return timeTaken/1000000;
    }

    /**
     * Calculates the number of operations (copy/swap/compare) performed by the sorting algorithm
     *
     * @return int: number of operations.
     */
    @Override
    public int numberOfOperations() {
        return count;
    }
}
