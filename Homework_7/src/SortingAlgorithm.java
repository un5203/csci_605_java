/*
 * SortingAlgorithm.java
 *
 *  Version 1.01
 */

/**
 *
 * This interface defines the structure of a sorting algorithm
 *
 * @author  Ujwal Bharat
 * @author Yeshwanth Raja
 */

public interface SortingAlgorithm {

    /**
     * returns the name of the sorting algorithm
     * @return String
     */
    public String getName();

    /**
     * Takes an int array as a parameter and sorts it.
     * @param array - array to be sorted
     * @return int[] - returns sorted int array
     */
    public int[] sortArray(int[] array);

    /**
     * Calculates the time taken to sort an array in milliseconds
     * @return double: the time taken to sort an array in milliseconds
     */
    public double timeTakenToSort();

    /**
     * Calculates the number of operations (copy/swap/compare) performed by the sorting algorithm
     * @return int: number of operations.
     */
    public int numberOfOperations();

}
