/*
 * DataReader.java
 *
 * Version 1.0.1
 */


import java.io.*;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * This class reads data from a raw/serialized/compressed/uncompressed file.
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */


public class DataReader {
    /**
     * Main method that accepts inputs from console and finds the corresponding file to read.
     * @param args
     */
    public static void main(String[] args) {
        ObjectHandler data = new ObjectHandler();
        Scanner s = new Scanner(System.in);
        System.out.println("Enter Filename");
        String filename = s.next();
        System.out.println("Was the file saved as Raw data or Serialized object (RD/SO)");
        String choice1 = s.next();
        System.out.println("Is the data Compressed (Y/N)");
        String choice2 = s.next();
        try {
            // Decompressing Data
            if (choice2.equalsIgnoreCase("Y")) {
//                filename = filename + ".zip";
//                FileInputStream zipfile = new FileInputStream(filename);
                ZipInputStream zip = new ZipInputStream(new FileInputStream(filename));
                ZipEntry zipEntry = zip.getNextEntry();
                while (zipEntry != null) {
                    String fname = zipEntry.getName();
                    // THis is retrieve raw data from the file
                    if (choice1.equalsIgnoreCase("RD")) {
                        FileInputStream inputStream = new FileInputStream(fname);
                        data.readObject(inputStream);
                    }
                    // This is to retrieve serialized object
                    else {
                        FileInputStream inputStream = new FileInputStream(fname);
                        ObjectInputStream objectstream = new ObjectInputStream(inputStream);
                        ObjectHandler handler = (ObjectHandler) objectstream.readObject();
                        data.readObject(objectstream);
//                        System.out.print(handler.toString());
                    }
                    zipEntry= zip.getNextEntry();
                }
                zip.closeEntry();
                zip.close();
            }
            else{
                    // THis is retrieve raw data from the file
                    if (choice1.equalsIgnoreCase("RD")) {
                        FileInputStream inputStream = new FileInputStream(filename);
                        data.readObject(inputStream);
                    }
                    // This is to retrieve serialized object
                    else {
                        FileInputStream inputStream = new FileInputStream(filename);
                        ObjectInputStream objectstream = new ObjectInputStream(inputStream);
                        ObjectHandler handler = (ObjectHandler) objectstream.readObject();
                        System.out.print(handler.toString());
                    }
                }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


//        catch(CustomException ce){
//            ce.printStackTrace();
//        }
    }
}
