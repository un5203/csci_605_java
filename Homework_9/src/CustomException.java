/*
 * CustomException.java
 *
 * Version 1.0.1
 */

/**
 * This class handles any custom exceptions required by the other classes.
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */


public class CustomException extends Exception {
    /**
     * Custom Exception that accepts a Throwable and calls its corresponding super constructor.
     * @param th
     */
    public CustomException(Throwable th){
        super(th);
    }
}
