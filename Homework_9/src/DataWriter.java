/*
 * DataWriter.java
 *
 * Version 1.0.1
 */


import java.io.*;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * This class writes data to a raw/serialized/compressed/uncompressed file.
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */


public class DataWriter {
    /**
     * Main method that accepts inputs from console and writes to the corresponding file.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        ObjectHandler data = new ObjectHandler();
        Scanner s = new Scanner(System.in);
        System.out.println("Enter Name");
        String name = s.nextLine();
        data.setName(name);
        System.out.println("Enter Age");
        String age = s.nextLine();
        data.setAge(Integer.parseInt(age));
        System.out.println("Enter Message");
        String msg = s.nextLine();
        data.setMessage(msg);
        System.out.println("Enter Filename");
        String filename = s.nextLine();
        File f = new File(filename);
        System.out.println("How do u want to save the data (RD for Raw data or SO for Serialize object):");
        String choice1 = s.nextLine();
        // THis is to save the raw data to the file
        if (choice1.equalsIgnoreCase("RD")) {
            FileOutputStream file = new FileOutputStream(f);
            data.writeObject(file);
        }
        // THis is to save the serialized object
        else {
            FileOutputStream file = new FileOutputStream(f);
            ObjectOutputStream outputStream = new ObjectOutputStream(file);
            outputStream.writeObject(data);

        }
        System.out.println("Do u want to Compress data (Y/N): ");
        String choice2 = s.next();

        // This is to compress the data
        if (choice2.equalsIgnoreCase("Y")) {
            filename = filename + ".zip";
            FileOutputStream zipfile = new FileOutputStream(filename);
            ZipOutputStream zip = new ZipOutputStream(zipfile);
            ZipEntry zipEntry = new ZipEntry(f.getName());
            zip.putNextEntry(zipEntry);
            FileInputStream fileInputStream = new FileInputStream(f);
            byte[] buf = new byte[1024];
            int bytesRead;
            while ((bytesRead = fileInputStream.read(buf)) > 0) {
                zip.write(buf, 0, bytesRead);
            }
            zip.closeEntry();
            zip.close();
            zipfile.close();
        }
        
    }
}
