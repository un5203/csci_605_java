/*
 * ObjectHandler.java
 *
 * Version 1.0.1
 */


import java.io.*;

/**
 * This class is a template for reading and writing data using input and output streams
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */


public class ObjectHandler implements Serializable {

    private String name;
    private int age;
    private String message;

    /**
     * Getters and Setters for private variables
     * @return
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Empty parametersless Constructor.
     */
    public ObjectHandler() {

    }

    /**
     * Writes the object to an output stream.
     * @param output
     */
    public void writeObject(OutputStream output) {
        try {
            DataOutputStream os = new DataOutputStream(output);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
            bw.write(getName()+"\n"+String.valueOf(getAge())+"\n"+getMessage());
            bw.close();
        } catch (IOException ce) {
            ce.printStackTrace();
        }
    }

    /**
     * Reads the object from an input stream.
     * @param input
     */
    public void readObject(InputStream input) {
        try {
            ObjectHandler data = new ObjectHandler();
            DataInputStream is = new DataInputStream(input);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            data.setName(br.readLine());
            data.setAge(Integer.parseInt(br.readLine()));
            data.setMessage(br.readLine());
            System.out.print(data.toString());
            br.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * Prints out the object data to console.
     * @return
     */
    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        string.append("=======================================\n"+
                "Name: " + getName()+ "\n" + "Age: " + getAge() + "\n" + "Message: " + getMessage()
                     +"\n=======================================");
        return String.valueOf(string);
    }

}
