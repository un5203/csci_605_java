
import java.io.FileNotFoundException;
import java.util.Collection;

/**
 * The representation of a single configuration for a puzzle.
 * The Backtracker depends on these routines in order to
 * solve a puzzle.  Therefore, all puzzles must implement this
 * interface.
 *
 * @author sps (Sean Strout @ RIT CS)
 * @author jeh (James Heliotis @ RIT CS)
 */
public interface Configuration {
    /**
     * Get the collection of successors from the current one.
     *
     * @return All successors, valid and invalid
     */
    public Collection<Configuration> getSuccessors();

    /**
     * Is the current configuration valid or not?
     *
     * @return true if valid; false otherwise
     */
    public boolean isValid(int row, int col);

    /**
     *
     * @param Filename : name of file that contains commands
     * @throws FileNotFoundException : thrown if if cannot be found
     */
    public void readCommands(String Filename) throws FileNotFoundException;

    /**
     * The environment for users to input commands manually
     */
    public void inputCommand();

    /**
     * Is the current configuration a goal?
     * @return true if goal; false otherwise
     */
    public boolean isGoal();
}
