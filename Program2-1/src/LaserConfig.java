/**
 * Created by schyler on 4/8/2016.
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Scanner;

    /**
     *  The full representation of a configuration in the TentsAndTrees puzzle.
     *  It can read an initial configuration from a file, and supports the
     *  Configuration methods necessary for the Backtracker solver.
     *
     *  @author: Sean Strout @ RIT CS
     *  @author: Schyler Sapinski
     */
    public class LaserConfig implements Configuration {
        // INPUT CONSTANTS
        /** An empty cell */
        public final static char EMPTY = '.';
        /** A cell occupied with a laser beam */
        public final static char BEAM = '*';
        /** A cell occupied with a laser */
        public final static char LASER = 'L';

        // OUTPUT CONSTANTS
        /** A horizontal divider */
        public final static char HORI_DIVIDE = '-';
        /** A vertical divider */
        public final static char VERT_DIVIDE = '|';

        //Position in Grid
        public int row = 0;
        public int col = 0;

        // Grid with Dimension
        public Character[][] config;
        public int dim_col;
        public int dim_row;


        /**
         * Construct the initial configuration from an input file whose
         * content are, for example:<br>
         * <tt><br>
         * 3 3       # row and col
         * . 1 .    # "1" can power 1 Laser
         * X . .    # "X" can power any number of lasers
         * . 0 .    # "0" can power 0 lasers
         * </tt><br>
         * @param filename the name of the file to read from
         * @throws FileNotFoundException if the file is not found
         */
        public LaserConfig(String filename) throws FileNotFoundException {
            Scanner in = new Scanner(new File(filename));
            dim_row = Integer.parseInt(in.next());
            dim_col = Integer.parseInt(in.next());
            config = new Character[dim_row][dim_col];

            for (int i = 0; i < dim_row; i++)
            {
                for (int j = 0; j < dim_col; j++)
                {
                    char spot = in.next().charAt(0);
                    if(spot=='X' || spot=='0' || spot=='1' || spot=='2' || spot== '3' || spot=='4' || spot==EMPTY || spot==LASER || spot==BEAM) {
                        config[i][j] = spot;
                    }
                    else {
                        config[i][j] = EMPTY;
                    }
                }
            }
            in.close();
        }

        /**
         * Copy constructor.  Takes a config, other, and makes a full "deep"
         * copy
         * of its instance data.
         * @param other the config to copy
         */
        public LaserConfig(LaserConfig other) {
            dim_row = other.dim_row;
            dim_col = other.dim_col;
            col = other.col;
            row = other.row;
            config = new Character[dim_row][dim_col];

            for (int i = 0; i < dim_row; i++)
            {
                for (int j = 0; j < dim_col; j++)
                {
                    config[i][j] = other.config[i][j];
                }
            }

        }

        @Override
        public Collection<Configuration> getSuccessors() {
            Collection<Configuration> temp = new ArrayList<Configuration>();
            return temp;
            //TODO
        }

        @Override
        /**
         * Checks current position to see if a laser can be placed there.
         * @param row row of current tile
         * @param col column of current tile
         * @return boolean if a laser can be placed at checked tile
         */
        public boolean isValid(int row, int col) {
            if (row < 0 || row > dim_row - 1)
            {
                return false;
            }
            if (col < 0 || row > dim_col - 1)
            {
                return false;
            }
            if (config[row][col].equals(EMPTY))
            {
                if (row == 0 && col == 0)
                {
                    if (config[row+1][col].equals("0") || config[row][col+1].equals("0"))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else if (row == 0 && col == dim_col - 1)
                {
                    if (config[row+1][col].equals("0") || config[row][col-1].
                            equals("0"))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else if (row == dim_row - 1 && col == 0)
                {
                    if (config[row-1][col].equals("0") || config[row][col+1]
                            .equals("0"))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else if (row == dim_row - 1 && col == dim_col)
                {
                    if (config[row-1][col].equals("0") || config[row][col-1]
                            .equals("0"))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else if (row == 0 && col != dim_col - 1 && col!= 0)
                {
                    if (config[row+1][col].equals("0") || config[row][col+1].
                            equals("0") || config[row][col-1].equals("0"))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else if (row == dim_row - 1 && col != 0 && col != dim_col -1)
                {
                    if (config[row-1][col].equals("0") || config[row][col+1].
                            equals("0") || config[row][col-1].equals("0"))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else if (row != dim_row - 1 && row != 0 && col == dim_col - 1)
                {
                    if (config[row+1][col].equals("0") || config[row-1][col].
                            equals("0") || config[row][col-1].equals("0"))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else if (row != dim_row - 1 && row != 0 && col == 0)
                {
                    if (config[row+1][col].equals("0") || config[row-1][col].
                            equals("0") || config[row][col+1].equals("0"))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    if (config[row+1][col].equals("0") || config[row-1][col].
                            equals("0") || config[row][col+1].
                            equals("0") || config[row][col-1].equals("0"))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            return false;
        }


        public boolean isGood(){
            return false;
        }

        public boolean isOk(int i, int j){

            return false;
        }

        public boolean isCorner(int i, int j){
            return (i== dim_row-1 || i==0) && (j== dim_col-1 || j==0);
        }

        public boolean isEdge(int i, int j){
            if(isCorner(i,j)){
                return false;
            }
            else if(isMiddle(i,j)){
                return false;
            }
            else{
                return true;
            }
        }

        public boolean isMiddle(int i, int j){
            return ((i!=0 && i!= dim_row-1) && (j!=0 && j!= dim_col-1));
        }


        public boolean cornerCheck(int i, int j){

            return false;
        }

        public String whichCorner(int i, int j){
            if(i==0 && j==0){
                return "top_left";
            }
            else if(i==0 && j!=0){
                return "top_right";
            }
            else if(i!=0 && j==0){
                return "bottom_left";
            }
            else{
                return "bottom_right";
            }
        }

        public String whichEdge(int i, int j){
            if(i==0){
                return "top_edge";
            }
            else if(j==0){
                return "left_edge";
            }
            else if(i== dim_row-1){
                return "bottom_edge";
            }
            else{
                return "right_edge";
            }
        }

        public boolean edgeCheck(int i, int j){
            return false;
        }

        public boolean middleCheck(int i, int j){
            return false;
        }

        public int lasersAroundPillar(int i, int j){
            int count = 0;
            if(isCorner(i,j)){
                switch(whichCorner(i,j)){
                    case "top_left":
                }
            }

           return count;
        }


        public Character right(int i, int j){
            try{
                return config[i][j+1];
            }
            catch (ArrayIndexOutOfBoundsException ae){
                return null;
            }

        }

        public Character left(int i, int j){
            try{
                return config[i][j-1];
            }
            catch (ArrayIndexOutOfBoundsException ae){
                return null;
            }

        }

        public Character bottom(int i, int j){
            try{
                return config[i+1][j];
            }
            catch (ArrayIndexOutOfBoundsException ae){
                return null;
            }

        }

        public Character top(int i, int j){
            try{
                return config[i-1][j];
            }
            catch (ArrayIndexOutOfBoundsException ae){
                return null;
            }
        }


        /**
         * Check if
         * @return
         */



        public boolean checkForEmpty(){
            return false;
        }

        public boolean areLasersValid(){
            return false;
        }

        public boolean keepTrackofPillars(){
            return false;
        }

        @Override
        /**
         * Checks to see if vault is correct
         * REDO THIS
         * @return A boolean if there are no conflicts with lasers and pillars
         */
        public boolean isGoal(){
            int laserCount = 0;
            for (int r = 0; r < dim_row; r++) {
                for (int c = 0; c < dim_col; c++)
                {
                    if (config[r][c].equals(EMPTY)) {
                        System.out.println("Error verifying at (" + r + ", "
                                + c + ")");
                        System.out.println(toString());
                        return false;
                    }
                    if (!config[r][c].equals(BEAM) && !config[r][c].equals(LASER)){
                        if (r == 0 && c ==0){
                            if (config[r][c+1].equals(LASER)){
                                laserCount++;
                            }else if (config[r+1][c].equals(LASER)){
                                laserCount++;
                            }
                        }else if(r == 0 && c == dim_col-1){
                            if (config[r][c-1].equals(LASER)){
                                laserCount++;
                            }else if (config[r+1][c].equals(LASER)){
                                laserCount++;
                            }
                        }else if(r == dim_row - 1 && c == 0){
                            if (config[r][c+1].equals(LASER)){
                                laserCount++;
                            }else if (config[r-1][c].equals(LASER)){
                                laserCount++;
                            }
                        }else if(r == dim_row - 1 && c == dim_col){
                            if (config[r][c+1].equals(LASER)){
                                laserCount++;
                            }else if (config[r-1][c].equals(LASER)){
                                laserCount++;
                            }
                        }else if(r == 0 && c != dim_col - 1 && c != 0){
                            if (config[r][c-1].equals(LASER)){
                                laserCount++;
                            }else if (config[r+1][c].equals(LASER)) {
                                laserCount++;
                            }else if (config[r][c+1].equals(LASER)){
                                laserCount++;
                            }
                        }else if(r == dim_row - 1 && c != 0 && c != dim_col - 1)
                        {
                            if (config[r+1][c].equals(LASER)){
                                laserCount++;
                            }
                            else if (config[r][c+1].equals(LASER)) {
                                laserCount++;
                            }
                            else if (config[r][c-1].equals(LASER)) {
                                laserCount++;
                            }
                        }else if(r != dim_row - 1 && r != 0 && c == dim_col - 1)
                        {
                            if (config[r+1][c].equals(LASER)){
                                laserCount++;
                            }else if (config[r-1][c].equals(LASER)){
                                laserCount++;
                            }else if (config[r][c-1].equals(LASER)) {
                                laserCount++;
                            }
                        }else if(r != dim_row - 1 && r != 0 && c == 0){
                            if (config[r+1][c].equals(LASER)){
                                laserCount++;
                            }else if (config[r-1][c].equals(LASER)) {
                                laserCount++;
                            }else if (config[r][c+1].equals(LASER)) {
                                laserCount++;
                            }
                        }else{
                            if (config[r+1][c].equals(LASER)){
                                laserCount++;
                            }else if (config[r-1][c].equals(LASER)) {
                                laserCount++;
                            }else if (config[r][c+1].equals(LASER)) {
                                laserCount++;
                            }
                            else if (config[r][c-1].equals(LASER)){
                                laserCount++;
                            }
                        }
                        if (!(laserCount >= config[r][c] - 48)){
                            System.out.println("Error verifying at (" + r +
                                    ", "
                                    + c + ")");
                            return false;
                        }
                    }
                }
            }
            System.out.println("Safe is fully verified!");
            return true;
        }

        @Override
        /**
         * Displays the vault
         * @return A string representation of the vault
         */
        public String toString() {
            String fin = "  ";
            for (int i = 0; i < dim_col; i++)
            {
                if (i >= 10)
                {
                    fin += " " + String.valueOf(i).charAt(1);
                }
                else
                {
                    fin += " " + i;
                }
            }
            fin += "\n" + "   ";
            for (int i = 0; i < (dim_col*2)-1; i++)
            {
                fin += HORI_DIVIDE;
            }
            fin += " \n";
            for (int i = 0; i < dim_row; i++)
            {
                if (i >= 10)
                {
                    fin += String.valueOf(i).charAt(1) + " " + VERT_DIVIDE;
                }
                else
                {
                    fin += i + " " + VERT_DIVIDE;
                }
                for (int j = 0; j < dim_col; j++)
                {
                    fin += config[i][j] + " ";
                }
                if (i != dim_row - 1)
                {
                    fin += "\n";
                }
            }
            return fin;
        }

        @Override
        /**
         * Reads input commands and executes them.
         * @param filename a file containing commands.
         * @return Executed action based on the read command
         */
        public void readCommands(String filename) throws FileNotFoundException
        {
            FileInputStream fileStr = new FileInputStream(filename);
            Scanner in = new Scanner(fileStr);
            while (in.hasNextLine())
            {
                System.out.print(">");
                String[] next = in.nextLine().split(" ");
                if (next.length != 2)
                {
                    System.out.println("Incorrect coordinates");
                }
                if (next[0].charAt(0) == 'a')
                {
                    addLaser(Integer.parseInt(next[1]), Integer.parseInt(
                            next[2]));
                }
                else if (next[0].charAt(0) == 'd')
                {
                    System.out.println(toString());
                }
                else if (next[0].charAt(0) == 'h')
                {
                    help();
                }
                else if (next[0].charAt(0) == 'q')
                {
                    return;
                }
                else if (next[0].charAt(0) == 'r')
                {
                    removeLaser(Integer.parseInt(next[1]), Integer.parseInt(
                            next[2]));
                }
                else if (next[0].charAt(0) == 'v')
                {
                    isGoal();
                }
                else
                {
                    String command = "";
                    for (String x : next)
                    {
                        command += x + " ";
                    }
                    System.out.println("Unrecognized command: " + command);
                }
            }
        }

        /**
         * Prints the list of possible command inputs
         * @return: void
         */
        public void help()
        {
            System.out.println(" a|add r c: Add laser to (r,c)");
            System.out.println("d|display: Display safe");
            System.out.println("h|help: Print this help message");
            System.out.println("q|quit: Exit program");
            System.out.println("r|remove r c: Remove laser from (r,c)");
            System.out.println("v|verify: Verify safe correctness");
            System.out.println(">");
        }


        /**
         * Adds a laser at a certain index in config
         * @param row: the row index to insert the laser
         * @param col: the col index to insert the laser
         * @return: void
         */
        public void addLaser(int row, int col)
        {
            if (isValid(row, col))
            {
                addBeem(row, col);
                System.out.println("Laser added at: (" + row + ", " +
                        col + ")");
                System.out.println(toString());
            }
            else
            {
                System.out.println("Error adding laser at: (" + row + ", " +
                        col + ")");
                System.out.println(toString());
            }
        }


        /**
         * Removes the laser at a certain index in config
         * @param row: the row index to remove the laser
         * @param col: the col index to remove the laser
         * @return: void
         */
        public void removeLaser(int row, int col)
        {
            if (row < 0 || row > dim_row - 1)
            {
                System.out.println("Error removing laser at: (" + row + ", " +
                        col + ")");
                System.out.println(toString());
                return;
            }
            if (col < 0 || row > dim_col - 1)
            {
                System.out.println("Error removing laser at: (" + row + ", " +
                        col + ")");
                System.out.println(toString());
                return;
            }
            if (config[row][col].equals(LASER))
            {
                removeBeem(row, col);
                for (int i = 0 ; i < dim_row; i++)
                {
                    for (int j = 0; j < dim_col; j++)
                    {
                        if (config[i][j].equals(LASER))
                        {
                            addBeem(i, j);
                        }
                    }
                }
                System.out.println("Laser removed at: (" + row + ", " +
                    col + ")");
                System.out.println(toString());
            }
            else
            {
                System.out.println("Error removing laser at: (" + row + ", " +
                        col + ")");
                System.out.println(toString());
            }

        }

        /**
         * Adds beems to config from the laser at the index [row][col]
         * @param row: the row index to insert the beems in the cardinal
         *           directions
         * @param col: the col index to insert the beems in the cardinal
         *           directions
         * @return: void
         */
        public void addBeem(int row, int col)
        {
            int col_left = col - 1;
            int col_right = col + 1;
            int row_down = row - 1;
            int row_up = row + 1;
            if (!(col_left < 0))
            {
                while (!Character.isDigit(config[row][col_left]) && col_left
                        >= 0 && !config[row][col_left].equals("X"))
                {
                    config[row][col_left] = BEAM;
                    col_left--;
                    if (col_left < 0)
                    {
                        break;
                    }
                }

            }

            if (!(col_right >= dim_col -1))
            {
                while (!Character.isDigit(config[row][col_right]) && col_right
                        <= dim_col - 1 && !config[row][col_right].equals("X"))
                {
                    config[row][col_right] = BEAM;
                    col_right++;
                    if (col_right > dim_col - 1)
                    {
                        break;
                    }
                }
            }
            if (!(row_up >= dim_row - 1))
            {
                while (!Character.isDigit(config[row_up][col]) &&
                        row_up <= dim_row - 1 &&
                        !config[row_up][col].equals("X"))
                {
                    config[row_up][col] = BEAM;
                    row_up++;
                    if (row_up >= dim_row -1) {
                        break;
                    }
                }
            }
            if (!(row_down < 0))
            {
                while (!Character.isDigit(config[row_down][col]) &&
                        row_down >= 0 && !config[row_down][col].equals("X"))
                {
                    config[row_down][col] = BEAM;
                    row_down--;
                    if (row_down < 0)
                    {
                        break;
                    }
                }
            }

            config[row][col] = LASER;
        }

        /**
         * Removes beems to config from the laser at the index [row][col]
         * @param row: the row index to remove the beems in the cardinal
         *           directions
         * @param col: the col index to remove the beems in the cardinal
         *           directions
         * @return: void
         */
        public void removeBeem(int row, int col)
        {
            int col_left = col - 1;
            int col_right = col + 1;
            int row_down = row - 1;
            int row_up = row + 1;
            if (!(col_left < 0))
            {
                while (!Character.isDigit(config[row][col_left]) && col_left
                        >= 0 && !config[row][col_left].equals("X"))
                {
                    config[row][col_left] = EMPTY;
                    col_left--;
                    if (col_left < 0)
                    {
                        break;
                    }
                }

            }

            if (!(col_right >= dim_col -1))
            {
                while (!Character.isDigit(config[row][col_right]) && col_right
                        <= dim_col - 1 && !config[row][col_right].equals("X"))
                {
                    config[row][col_right] = EMPTY;
                    col_right++;
                    if (col_right > dim_col - 1)
                    {
                        break;
                    }
                }
            }
            if (!(row_up >= dim_row - 1))
            {
                while (!Character.isDigit(config[row_up][col]) &&
                        row_up <= dim_row - 1 &&
                        !config[row_up][col].equals("X"))
                {
                    config[row_up][col] = EMPTY;
                    row_up++;
                    if (row_up >= dim_row -1) {
                        break;
                    }
                }
            }
            if (!(row_down < 0))
            {
                while (!Character.isDigit(config[row_down][col]) &&
                        row_down >= 0 && !config[row_down][col].equals("X"))
                {
                    config[row_down][col] = EMPTY;
                    row_down--;
                    if (row_down < 0)
                    {
                        break;
                    }
                }
            }
            config[row][col] = EMPTY;
        }

        @Override
        /**
         * Executes commands based on what is input.
         * @return void
         */
        public void inputCommand()
        {

            Scanner in = new Scanner(System.in);
            while (true)
            {
                System.out.print(">");
                while (in.hasNextLine())
                {
                    String[] next = in.nextLine().split(" ");
                    if (next[0].charAt(0) == 'a')
                    {
                        addLaser(Integer.parseInt(next[1]),
                                Integer.parseInt(next[2]));
                    }
                    else if (next[0].charAt(0) == 'd')
                    {
                        System.out.println(toString());
                    }
                    else if (next[0].charAt(0) == 'h')
                    {
                        help();
                    }
                    else if (next[0].charAt(0) == 'q')
                    {
                        return;
                    }
                    else if (next[0].charAt(0) == 'r')
                    {
                        removeLaser(Integer.parseInt(next[1]),
                                Integer.parseInt(next[2]));
                    }
                    else if (next[0].charAt(0) == 'v')
                    {
                        isGoal();
                    }
                }
            }
        }
    }
