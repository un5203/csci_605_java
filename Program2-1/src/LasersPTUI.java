import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by schyler on 4/7/2016.
 * @author Schyler Sapinski
 * @author Amanda Ramos
 * @author Emmanuel Palad
 */
public class LasersPTUI {
    public static void main(String[] args) throws FileNotFoundException{

        if (args.length == 1)
        {
            // construct the initial configuration from the file
            Configuration init = new LaserConfig(args[0]);
            System.out.println(init.toString());
            init.inputCommand();
        }
        else if (args.length == 2)
        {
            Configuration init = new LaserConfig(args[0]);
            System.out.println(init.toString());
            init.readCommands(args[1]);
            init.inputCommand();
        }
        else
        {
            System.out.println("Usage: java LasersPTUI safe-file [input]");
            return;
        }

    }
}