package com.homework2;

import java.util.Scanner;

/*
 * DateCheck.java
 *
 *  Version 1.01
 */

/**
 *
 * DateCheck class accepts a date from console in a particular
 * format and displays it in human readable form.
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */


public class DateCheck {
    public static void main(String args[]){
        String month = null;
        String suffix=null;
        Scanner s = new Scanner(System.in);
        System.out.println("Enter Date in this Format YYYY/MM/DD");
        String date = s.nextLine();

        int y = Integer.parseInt(date.substring(0,4));
        int m =Integer.parseInt(date.substring(5,7));
        int d = Integer.parseInt(date.substring(8,10));

        if(m>12){
            System.out.println("Error: There are only 12 months in a year !");
            return;
        }

        if(d<1 || d>31){
            System.out.println("Error: Date cannot be greater than 31");
            return;
        }

        switch (m) {
            case 1:
                month = "January";
                break;
            case 2:
                month = "February";
                break;
            case 3:
                month = "March";
                break;
            case 4:
                month = "April";
                break;
            case 5:
                month = "May";
                break;
            case 6:
                month = "June";
                break;
            case 7:
                month = "July";
                break;
            case 8:
                month = "August";
                break;
            case 9:
                month = "September";
                break;
            case 10:
                month = "October";
                break;
            case 11:
                month = "November";
                break;
            case 12:
                month = "December";
                break;
        }

        switch(d%10){
            case 0:suffix="th";
                break;
            case 1:suffix="st";
                break;
            case 2:suffix="nd";
                break;
            case 3:suffix="rd";
                break;
            case 4:suffix="th";
                break;
            case 5:suffix="th";
                break;
            case 6:suffix="th";
                break;
            case 7:suffix="th";
                break;
            case 8:suffix="th";
                break;
            case 9:suffix="th";
                break;
        }
        System.out.println(month+" "+d+suffix+","+" "+y);

    }
}

