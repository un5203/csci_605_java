package com.homework2;
import java.util.Scanner;

/*
 * PrimeCheck.java
 *
 *  Version 1.01
 */

/**
 *
 * PrimeCheck class accepts a number from the console
 * and checks its primality using for, while or do while loops
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */



public class PrimeCheck {
    public static void main(String args[]) {
        boolean flag = true;
        int temp;
        String looptype;
        Scanner s = new Scanner(System.in);
        System.out.println("Enter number to check");
        int n = s.nextInt();
        System.out.println("Type of loop (F for forloop , W for Whileloop and D for DoWhile Loop)");
        looptype = s.next();
        if (looptype.equals("F")) {
            long start = System.nanoTime();
            for (int i = 2; i <= n/2; i++) {
                temp = n % i;
                if (temp == 0) {
                    flag = false;
                    break;
                }
            }
            long stop = System.nanoTime();
            double elapsedTime = (stop-start)/1e6;
            timerMessage(elapsedTime,flag);

        } else if (looptype.equals("W")) {
            long start = System.nanoTime();
            int i = 2;
            while (i <= n / 2) {
                temp = n % i;
                if (temp == 0) {
                    flag = false;
                    break;
                }
                i++;
            }
            long stop = System.nanoTime();
            double elapsedTime = (stop-start)/1e6;
            timerMessage(elapsedTime,flag);

        } else if (looptype.equals(("D"))) {
            long start = System.nanoTime();
            int i = 2;
            do {
                temp = n % i;
                if (temp == 0) {
                    flag = false;
                    break;
                }
                i++;
            } while (i <= n / 2);
            long stop = System.nanoTime();
            double elapsedTime = (stop-start)/1e6;
            timerMessage(elapsedTime,flag);
        }

    }

    public static void timerMessage(double elapsedTime,boolean flag){
        if (flag) {
            System.out.println("Its a Prime Number");
            System.out.println("It took " + elapsedTime + " milliseconds to compute the answer !");
        }
        else {
            System.out.println("Not a Prime Number");
            System.out.println("It took " + elapsedTime + " milliseconds to compute the answer !");
        }
    }
}
