package com.homework2;

import java.util.Scanner;

public class PrimeFactors {
    public static void main(String args[]) {
        int j=0;
        Scanner s = new Scanner(System.in);
        System.out.println("Enter number");
        int n = s.nextInt();
        int[] factors = new int[n+1];
        for (int i = 2; i <= n; i++) {
            while(n%i==0){
                factors[j]=i;
                n/=i;
                j++;
            }
        }
        System.out.println("Prime factors are");
        for (int i=0;i<factors.length;i++) {
            if(i==0){
                System.out.print(factors[i]);
            }
            if(i > 0 && factors[i] !=0){
                System.out.print(" * "+ factors[i]);
            }
        }
    }
}
