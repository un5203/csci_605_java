package com.homework2;

import java.util.Scanner;

public class Sieve {
    static int num = 1000000;
    private static Boolean primes[] = new Boolean[num+1];
    static{
        for (int i = 2; i <= num; i++) {
            primes[i] = true;
        }
    }
    public static void main(String args[]) {
        System.out.println("enter number to check primality");
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        for (int i = 2; i * i <= num; i++) {
            if (primes[i]) {
                for (int j = i; i * j <= num; j++)
                    primes[i * j] = false;
            }
        }
        if (primes[n])
            System.out.println("Prime number");
        else
            System.out.println("Not a PrimeCheck number");
    }
}
