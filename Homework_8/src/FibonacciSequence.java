/**
 * FibonacciSequence.java
 * Version: 1.0
 */

/**
 * This class calculates the sum of even-valued terms of a Fibonacci sequence where the highest term does not exceed
 * 4 million.
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */

public class FibonacciSequence {

    /**
     * Main method that calculates the sum of even-valued terms of a Fibonacci sequence where the highest term does
     * not exceed 4 million.
     * @param args
     */
    public static void main(String[] args) {
        int count = 2;
        int a = 1;
        int b = 2;
        int temp = 0;
        while (b < 4000000){
            temp = a + b;
            if(temp%2==0){
                count+= temp;
            }
            a = b;
            b = temp;
        }
        System.out.println(count);
    }
}

//4613732
