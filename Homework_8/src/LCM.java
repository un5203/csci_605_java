/**
 * LCM.java
 * Version: 1.0
 */

/**
 * This class calculates the least common multiple of all the numbers from 1 to 20.
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */
public class LCM {
    /**
     * Calculates the greatest common divisor using Euclid's division algorithm.
     * @param a
     * @param b
     * @return long: returns the gcd.
     */
    public static long gcd(long a, long b) {
        if ( a == 0 ) return 1;
        if( b % a == 0){
            return a;
        }
        else{
            return gcd(b%a,a);
        }
    }

    /**
     * Calculates the least common multiple of two numbers.
     * @param a
     * @param b
     * @return long: returns the lcm.
     */
    public static long lcm(long a, long b){
        return (a*b)/gcd(a,b);
    }

    /**
     * main method that calculates the lcm of all the numbers from 1 to 20.
     * @param args
     */
    public static void main(String[] args) {
        long temp = 20;
        for(int i = 19;i>0;i--){
            temp = lcm(i,temp);
        }
        System.out.println(temp);
    }
}