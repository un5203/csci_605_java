/**
 * SumMultiple.java
 * Version: 1.0
 */

/**
 * This class calculates the sum of all the multiples of 3 or 5 below 1000.
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */

public class SumMultiple {
    private static int count = 0;

    /**
     * Main method that calculates the sum of all the multiples of 3 or 5 below 1000.
     * @param args
     */
    public static void main(String[] args) {
        for (int i = 1; i < 1000; i++) {
            if( i%3 == 0 || i% 5 == 0){
                count += i;
            }
        }
        System.out.println(count);
    }
}