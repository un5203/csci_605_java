/**
 * LargestPrimeFactor.java
 * Version: 1.0
 */

/**
 * This class calculates the largest prime factor of 600851475143.
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */

public class LargestPrimeFactor {
    /*
     * This is the main method
     * @param: String[] args
     * @return: void
     */
    public static void main(String[] args) {
        double theNumber = 600851475143D;
        int arraySize = (int) Math.sqrt(theNumber);
        int largestPrimeFactor = 0;
        boolean[] isPrime = new boolean[arraySize];

        //assumes every number is prime.
        for (int i = 2; i < arraySize; i++) {
            isPrime[i] = true;
        }

        //sets the multiples of all primes to false.
        for (int i = 2; i * i < arraySize; i++) {
            if (isPrime[i]) {
                for (int j = 1; i * j < arraySize; j++) {
                    isPrime[i * j] = false;
                }
            }
        }

        //Calculates the largest prime factor and keeps track of it.
        for (int i = 2; i < isPrime.length; i++) {
            if (isPrime[i] && theNumber % i == 0) {
                largestPrimeFactor = i;
            }
        }
        System.out.println(largestPrimeFactor);
    }
}
