/**
 * SumSquareDifference.java
 * Version: 1.0
 */

/**
 * This class calculates the difference between the square of the sum of hundred numbers and the sum of squares of 100
 * numbers.
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */

public class SumSquareDifference {
    /**
     * Main method that calculates the difference between the square of the sum of hundred numbers and the sum of
     * squares of 100 numbers.
     * @param args
     */
    public static void main(String[] args) {
        long sumOfSquares = 0;
        long squareOfSum = 0;
        for(int i = 1;i<101;i++){
            sumOfSquares += Math.pow(i,2);
            squareOfSum += i;
        }
        System.out.format("%.0f",Math.pow(squareOfSum,2)-sumOfSquares);
    }
}
