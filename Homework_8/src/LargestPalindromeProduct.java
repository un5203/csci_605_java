/**
 * LargestPalindromeProduct.java
 * Version: 1.0
 */

/**
 * This class calculates the largest palindrome possible by multiple two 3-digit numbers.
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */

public class LargestPalindromeProduct {
    /**
     * Returns the reverse of a number
     * @param num
     * @return reverse of the input parameter as an integer
     */
    public static int reverseNumber(int num){
        int reverse = 0;
        while( num != 0 )
        {
            reverse = reverse * 10;
            reverse = reverse + num%10;
            num = num/10;
        }
        return reverse;
    }

    /**
     * Checks whether a number is palindrome or not.
     * @param str
     * @return boolean: True if palindrome
     */
    public static boolean isPalindrome(int str){
        int reverse = reverseNumber(str);
        if(reverse == str){
            return true;
        }
        return false;
    }

    /**
     * Main method that calculates the largest palindrome produced by multiplying two 3-digit numbers.
     * @param args
     */
    public static void main(String[] args) {
        int temp = 0;
        int largest = 0;
        for(int i = 100;i<1000;i++){
            for(int j = 100;j<1000;j++){
                if(isPalindrome(i*j)){
                    temp = i*j;
                    if(temp > largest){
                        largest = temp;
                    }
                }
            }
        }
        System.out.println(largest);
    }
}
