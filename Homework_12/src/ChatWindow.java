/**
 * ChatWindow.java
 * Version 1.0.1
 */

import java.util.Scanner;

/**
 * Implements the view part of MVC pattern. Creates the user interface.
 */
public class ChatWindow implements Observer{

    private ChatController controller;

    /**
     * Constructor
     * @param controller
     */
    public ChatWindow(ChatController controller){
        this.controller = controller;
        this.controller.subscribe(this);
    }

    /**
     * Gets the username from the user.
     * @return
     */
    public String askForUsername(){
        System.out.println("Please enter your username:");
        return getInputFromUser();
    }

    /**
     * Displays a menu of options
     */
    public void showMenu(){
        System.out.println("\nPlease select one of the following:");
        System.out.println("l - list the online users");
        System.out.println("m - send a message");
        System.out.println("q - quit the program\n");
    }

    /**
     * Gets input from user.
     * @return
     */
    public String getInputFromUser(){
        Scanner input = new Scanner(System.in);
        return input.nextLine();
    }

    /**
     * Displays a message
     * @param message
     */
    public synchronized void displayMessage(String message){
        printLooper("#",message.length());
        System.out.println(message);
        printLooper("#",message.length());
    }

    /**
     * Prints a character a certain number of times
     * @param character
     * @param noOfTimes
     */
    public void printLooper(String character, int noOfTimes){
        for(int i=0;i<noOfTimes;i++){
            System.out.print(character);
        }
        System.out.println("");
    }

    /**
     * Display an error message
     * @param message
     */
    public synchronized void displayErrorMessage(String message){
        printLooper("!",message.length());
        System.out.println(message);
        printLooper("!",message.length());
    }

    /**
     * Start the user interface.
     * @throws Exception
     */
    public void start() throws Exception {
        while(true){
            String username = askForUsername();
            if(controller.addUser(username)){
                System.err.flush();
                break;
            }
        }
        boolean quit = false;
        while(!quit){
            showMenu();
            String option = getInputFromUser();
            switch (option){
                case "l":
                    System.out.println("\n"+controller.showOnlineUsers()+"\n");
                    break;
                case "m":
                    sendingMessage();
                    break;
                case "q":
                    controller.quit();
                    System.out.println("Bye bye !!");
                    System.exit(0);
                    break;
                default: break;
            }
        }
    }

    /**
     * Starts the sequence of sending message to another user.
     */
    public void sendingMessage(){
        System.out.println("\nEnter the name of the person to send a message:");
        String toUser = getInputFromUser();
        System.out.println("Enter the message to send to "+toUser+" :");
        String message = getInputFromUser();
        controller.sendMessageTo(message,toUser);
    }

    /**
     * This method gets called when the Model sends a message.
     * @param message
     * @param flag
     */
    @Override
    public void update(String message,String flag) {
        if(flag.equals("error"))
            displayErrorMessage(message);
        else{
            displayMessage(message);
        }
    }

    /**
     * This method gets called when the user receives a message.
     */
    @Override
    public void youGotMail() {
        String message = controller.getMessages();
        if(message != null){
            printLooper("@",message.length());
            System.out.print(message);
            printLooper("@",message.length());
        }
        else{
            System.out.println(message);
        }
        showMenu();
    }
}
