/**
 * ChatterBox.java
 * Version 1.0.1
 */

/**
 * Main application that links the model, view and controller.
 */
public class ChatterBox {
    /**
     * Main function
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        ChatModel model = new ChatModel();
        ChatController controller = new ChatController(model);
        ChatWindow view = new ChatWindow(controller);
        view.start();
    }
}
