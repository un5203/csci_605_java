package v1; /**
 * v1.CustomException.java
 * Version1.01
 */

/**
 * This class throws a message as custom exception
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */
public class CustomException extends Exception {
    public CustomException(String message){
        System.out.println(message);
    }
}
