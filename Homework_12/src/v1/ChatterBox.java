package v1; /**
 * v1.ChatterBox.java
 */

/**
 * This class checks the MVC pattern implementation
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */

public class ChatterBox {
    public static void main(String[] args){
        ChatMessages messages = new ChatMessages();
        ChatWindow ui = new ChatWindow();
        ChatController controller = new ChatController(ui,messages);
        controller.getUsername();
        controller.showMenu();
    }
}
