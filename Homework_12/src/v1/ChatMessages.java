package v1; /**
 * v1.ChatMessages.java
 * Version 1.01
 */

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This class acts as a model in MVC and manipulates data
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */
public class ChatMessages implements Runnable{
    private File inbox;
    private File messagesDir = new File("messages");
    private List<Observer> observers = new ArrayList<>();
    private long timestamp;

    public File getInbox(){
        return this.inbox;
    }

    public void createInboxFor(String filename,ChatMessages messages){
        messagesDir.mkdir();
        inbox = new File("messages/"+filename);
        try {
            inbox.createNewFile();
            Thread thread = new Thread(messages);
            thread.start();
            timestamp = inbox.lastModified();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void attach(Observer observer){
        observers.add(observer);
    }

    public void notifyAllObservers(){
        for(Observer observer: observers){
            observer.update();
        }
    }

    public boolean doesUserExist(String user){
        return listUsers().contains(user);
    }

    public void quit(){
        inbox.deleteOnExit();
    }

    public List<String> listUsers() {
        List<String> users = new ArrayList<>();
        for (File inbox : messagesDir.listFiles()){
            users.add(inbox.getName());
        }
        return users;
    }

    public String readMessage(){
        BufferedReader reader = null;
        String message = null;
        try{
            reader = new BufferedReader(new FileReader(inbox));
            message = reader.readLine();
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return message;
    }

    public void sendMessage(String fromUser, String toUser, String message) {
        if(fromUser.equals(toUser)) {
            System.out.println("You cannot send a message to yourself");
        }
        else{
            if(doesUserExist(toUser)){
                File file = new File("messages/" + toUser);
                try(BufferedWriter writer = new BufferedWriter(new FileWriter(file));) {
                    writer.write("@"+fromUser+":"+" "+message);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        while(true){
            if(inbox.exists() && timestamp != inbox.lastModified()){
                    notifyAllObservers();
                    timestamp = inbox.lastModified();
                }
            }
        }
}
