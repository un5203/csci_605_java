package v1; /**
 * v1.ChatWindow.java
 * Version 1.01
 */

import java.util.List;
import java.util.Scanner;

/**
 * This class acts like the view in the MVC pattern
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */
public class ChatWindow {

    /**
     * Receives the username from the user and sends it to the controller
     * @return
     */
    public String getUsername(){
        System.out.println("\n#######################################");
        return followCommand("Please enter your username:");
    }

    /**
     * Receives a message from the controller and prints it and returns the
     * input back to the controller
     * @param message
     * @return
     */
    public String followCommand(String message){
        System.out.println("\n#######################################");
        System.out.println("\n" +message);
        Scanner input = new Scanner(System.in);
        return input.nextLine();
    }

    /**
     * Displays various menu options and sends user's input back to the
     * controller
     * @return
     */
    public String showMenu(){
        System.out.println("\n#######################################");
        System.out.println("\nYou have 3 options to choose from:");
        System.out.println("o - List online users");
        System.out.println("m - Send a message to online users");
        System.out.println("q - Quit");
        Scanner input = new Scanner(System.in);
        return input.nextLine();
    }

    /**
     * Displays a custom message to the user that was passed from the controller
     * @param message
     */
    public void displayMessage(String message){
        System.out.println("\n#######################################");
        System.out.println("\n" + message);
    }

    /**
     * Displays a custom error message to the user that was passed from the
     * controller
     * @param errorMsg
     */
    public void errorMessage(String errorMsg){
        System.out.println("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("\n" + errorMsg);
        System.out.println("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    }

    /**
     * Displays list of online users
     * @param onlineUsers
     */
    public void showUsers(List<String> onlineUsers) {
        System.out.println("\n#######################################");
        System.out.println("\nUsers currently online:");
        System.out.println("\n");
        for(int i = 0;i<onlineUsers.size();i++){
            System.out.print("[ " + onlineUsers.get(i) + "] ");
        }
        System.out.println("\n#######################################");
    }
}

