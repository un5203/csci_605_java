package v1;/*
 * v1.ChatController.java
 *
 * Version 1.0.1
 */

import java.util.*;

/**
 * This class acts as a link between v1.ChatWindow and v1.ChatMessages
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */
public class ChatController extends Observer {
    private ChatWindow ui;
    private ChatMessages messages;
    private String currentUser;

    /**
     * Constructor
     * @param ui
     * @param messages
     */
    public ChatController(ChatWindow ui, ChatMessages messages) {
        this.ui = ui;
        this.messages = messages;
        this.messages.attach(this);
    }

    /**
     * Gets the username from the view
     */
    public void getUsername() {
        currentUser = ui.getUsername();
        messages.createInboxFor(currentUser,messages);
    }

    /**
     * lists the users in the view
     */
    public void listUsers(){
        List<String> onlineUsers = messages.listUsers();
        ui.showUsers(onlineUsers);
    }


    /**
     * shows the menu options that a user can select in the view
     */
    public void showMenu(){
        String option = ui.showMenu();
        switch(option){
        case "o":
                    listUsers();
                    showMenu();
                    break;

        case "m":
                    listUsers();
                    String user = ui.followCommand("Enter the username of the" +
                            " person you wish to send a message to:");
                    if (user.equals(currentUser)){
                        try {
                            throw new CustomException("You cannot send a message " +
                                    "to yourself");
                        } catch (CustomException e) {
//                            e.printStackTrace();
                        }
                        showMenu();
                    }
                    else{
                        if(messages.doesUserExist(user)){
                            String message = ui.followCommand("Enter your message");
                            messages.sendMessage(currentUser,user, message);
                            ui.displayMessage("Message sent !");
                            showMenu();
                        }
                        else{
                            try {
                                throw new CustomException("That user is not " +
                                        "currently online !");
                            } catch (CustomException e) {
//                                e.printStackTrace();
                            }
                            showMenu();
                        }
                    }
                    break;

        case "q":
                    messages.quit();
                    break;

        default:
                    ui.errorMessage("Invalid Option. Try again!");
                    showMenu();
                    break;
        }
    }

    /**
     * This method is called whenever the observed object is changed. An
     * application calls an <tt>Observable</tt> object's
     * <code>notifyObservers</code> method to have all the object's
     * observers notified of the change.
     */
    @Override
    public void update() {
        ui.displayMessage("You have received a new message!\n"+messages
                .readMessage()+"\n");
        showMenu();
    }
}
