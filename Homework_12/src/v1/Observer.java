package v1;

public abstract class Observer {
    public abstract void update();
}
