/**
 * ChatController.java
 * Version 1.0.1
 */

import java.io.IOException;

/**
 * This class links the ChatModel and ChatWindow files.
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */
public class ChatController {

    private ChatModel model;

    /**
     * Constructor
     * @param model
     * @throws Exception
     */
    public ChatController(ChatModel model) throws Exception {
        this.model = model;
    }

    /**
     * Links the view to the model for messages subscription.
     * @param view
     */
    public void subscribe(ChatWindow view){
        model.attachObserver(view);
    }

    /**
     * Adds the user to the list of online users.
     * @param username
     * @return
     */
    public boolean addUser(String username){
        try {
            model.addUser(username);
            return true;
        } catch (IOException e) {
            model.notifyObserver("Unable to create your session!","error");
            return false;
        } catch (ChatException e) {
            model.notifyObserver("User already exists! Try Again!","error");
            return false;
        }
    }

    /**
     * Sends a message to a particular user.
     * @param message
     * @param toUsername
     */
    public void sendMessageTo(String message, String toUsername){
        try {
            model.sendMessageTo(message,toUsername);
            model.notifyObserver("Your message has been successfully sent !",
                    "general");
        } catch (ChatException e) {
            model.notifyObserver("Cannot send a message to yourself!",
                    "error");
        } catch (IOException e) {
            model.notifyObserver("Unable to write your message! Try Again!",
                    "error");
        }
    }

    /**
     * Displays the list of online users.
     * @return
     */
    public String showOnlineUsers(){
        return model.showOnlineUsers();
    }

    /**
     * Quits the program after cleaning up files.
     */
    public void quit(){
        model.quit();
    }

    /**
     * Gets messages for the user.
     * @return
     */
    public String getMessages(){
        try {
            return model.getMessages();
        } catch (IOException e) {
            model.notifyObserver("Unable to get your messages!","error");
            return null;
        }
    }

}
