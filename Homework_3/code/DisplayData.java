/*
 * DisplayData.java
 *
 *  Version 1.01
 */

/**
 *
 * This class prints the details of an employee.
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */

public class DisplayData {

    //this method accepts an Employee object as an argument and modifies its contents and prints it.
    public void printEmployeeDetails(Employee employee){
        System.out.println("<==========================================>");
        System.out.println("Before modification");
        System.out.println("<==========================================>");
        System.out.println(employee.getName());
        System.out.println(employee.getSalary());
        System.out.println(employee.getAddress());
        System.out.println(employee.getDateOfBirth());
        System.out.println("\n \n");

        //Modifying the data
        employee.setName("Angela Martin");
        employee.setSalary(3500);
        employee.setAddress("Oscar's Closet, Scranton, Pennsylvania");
        employee.setDateOfBirth("3rd March, 1977");


        System.out.println("<==========================================>");
        System.out.println("After modification");
        System.out.println("<==========================================>");
        System.out.println(employee.getName());
        System.out.println(employee.getSalary());
        System.out.println(employee.getAddress());
        System.out.println(employee.getDateOfBirth());
        System.out.println("\n \n");
    }

    //main method
    public static void main(String[] args) {

        DisplayData dd = new DisplayData();
        Employee employee1 = new Employee("Andrew Bernard",5000,"Family Yacht, Scranton, Pennsylvania","12th May, 1972");
        dd.printEmployeeDetails(employee1);
        Clerk clerk1 = new Clerk();
        dd.printEmployeeDetails(clerk1);

        /*  printEmployeeDetails method accepts Employee object by default. However, since Clerk class has been
            inherited from Employee class, it can be subsituted here because of Polymorphism
            where any child class can be treated as an instance of the parent class.
        */

    }
}
