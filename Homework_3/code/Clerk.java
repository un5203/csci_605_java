/*
 * Clerk.java
 *
 *  Version 1.01
 */

/**
 *
 * This class creates an Clerk object which inherits from Employee class.
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */

public class Clerk extends Employee{
    //= {"Selling Paper","Managing inventory", "Making calls"};
    final private String[] tasks;
    final private int numberOfClients;
    private String position = "Junior Sales Associate";

    //accessors of the above fields
    public String getTasks(){
        String taskList = "";
        for(String t:tasks){
            taskList += t;
            if(!t.equals(tasks[tasks.length - 1])){
                taskList += ", ";
            }
        }
        return taskList;
    }

    public int getNumberOfClients(){
        return numberOfClients;
    }

    public String getPosition(){
        return position;
    }

    public void setPosition(String position){
        this.position = position;
    }



    //the following mutators don't work as the fields are finalized with the final keyword.
    //They can only be set inside a constructor
    /*
        public void setTasks(String[] tasks){
            this.tasks = tasks;
        }

        public void setNumberOfClients(String numberOfClients){
            this.numberOfClients = numberOfClients;
        }
    */

    //parameterless constructor
    public Clerk(){
        //this(new String[] {"Selling Paper","Managing inventory", "Making calls"},10,"Senior Sales Associate","Dwight Schrute", 4000,"Beet Farm, Pennsylvania","3rd April, 1976");
        super();
        this.tasks = new String[] {"Selling Paper","Managing inventory", "Making calls"};
        this.numberOfClients = 10;
    }


    //Constructor that takes some arguments
    public Clerk( String[] tasks,int numberOfClients, String position, String name, int salary, String address, String dateOfBirth){
        super(name,salary,address,dateOfBirth);
        this.tasks = tasks;
        this.numberOfClients = numberOfClients;
        this.setPosition(position);
    }

    //This function changes the content of a finalized object.
    public void changeFinalContents(String[] tasks){
        System.out.println("<==========================================>");
        System.out.println("Contents of the tasks array before modification");
        System.out.println("<==========================================>");

        for(String t:this.tasks){
            System.out.println(t);
        }

        //Even though tasks array on the object has been set to final, its contents
        //can still be changed as the only thing that cannot be changed on a finalized field
        //is its memory location.

        //this.tasks = new String[10];

        //For example.. uncommenting the previous line will throw an error as a finalized field cannot
        // be reinitialized.


        System.out.println("<==========================================>");
        System.out.println("Contents of the tasks array after modification");
        System.out.println("<==========================================>");
        for(int i = 0;i<tasks.length;i++){
            this.tasks[i] = tasks[i];
            System.out.println(this.tasks[i]);
        }

    }
    //main function
    public static void main(String[] args) {
        Clerk clerk1 = new Clerk();
        System.out.println("<==========================================>");
        System.out.println("Clerk 1");
        System.out.println("<==========================================>");
        System.out.println(clerk1.getName());
        System.out.println(clerk1.getPosition());
        System.out.println(clerk1.getTasks());
        System.out.println(clerk1.getSalary());
        System.out.println(clerk1.getDateOfBirth());
        System.out.println(clerk1.getAddress());
        System.out.println(clerk1.getNumberOfClients());
        System.out.println("<==========================================>");
        System.out.println("\n \n");
        System.out.println("After modifying clerk 1's changeable fields");
        System.out.println("<==========================================>");
        clerk1.setName("Pamela Beasly");
        clerk1.setPosition("Receptionist");
        clerk1.setDateOfBirth("16th November, 1980");
        clerk1.setAddress("Poor Richard's pub, Scranton, Pennsylvania");
        clerk1.setSalary(2000);

        /*
        The following two lines don't work as the "tasks" and "numberOfClients" fields have been set to final
        and they have no direct mutators. Tasks field has to be manually updated via loops. However numberOfClients
        cannot be changed as it is a primitive instance variable and can only be changed via a constructor

        clerk1.setTasks(new String[] {"Taking calls", "Taking minutes of meeting"});
        clerk1.setNumberOfClients(0)

        */


        System.out.println(clerk1.getName());
        System.out.println(clerk1.getPosition());
        System.out.println(clerk1.getTasks());
        System.out.println(clerk1.getSalary());
        System.out.println(clerk1.getDateOfBirth());
        System.out.println(clerk1.getAddress());
        System.out.println(clerk1.getNumberOfClients());




        Clerk clerk2 = new Clerk(new String[] {"Making coffee","Annoying customers"},5,"Senior Sales Associate","Dwight Schrute", 4000,"Beet Farm, Pennsylvania","3rd April, 1976");
        System.out.println("<==========================================>");
        System.out.println("Clerk 2");
        System.out.println("<==========================================>");
        System.out.println(clerk2.getName());
        System.out.println(clerk2.getPosition());
        System.out.println(clerk2.getTasks());
        System.out.println(clerk2.getSalary());
        System.out.println(clerk2.getDateOfBirth());
        System.out.println(clerk2.getAddress());
        System.out.println(clerk2.getNumberOfClients());
        System.out.println("<==========================================>");
        System.out.println("\n \n");
        String[] newTasks = {"Helping Michael Scott", "Preparing Conference Room"};
        clerk2.changeFinalContents(newTasks);
    }

}
