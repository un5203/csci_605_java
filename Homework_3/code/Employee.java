/*
 * Employee.java
 *
 *  Version 1.01
 */



/**
 *
 * This class creates an employee object and fill it with information.
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */


public class Employee {
    private String name;
    private int salary;
    private String address;
    private String dateOfBirth;

    //accessors and mutators for the above fields
    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public int getSalary(){
        return salary;
    }

    public void setSalary(int salary){
        this.salary = salary;
    }

    public String getAddress(){
        return address;
    }

    public void setAddress(String address){
        this.address = address;
    }

    public String getDateOfBirth(){
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth){
        this.dateOfBirth = dateOfBirth;
    }

    //initializing constructor
    public Employee(String name, int salary, String address, String dateOfBirth){
        this.name = name;
        this.salary = salary;
        this.address = address;
        this.dateOfBirth = dateOfBirth;
    }

    //parameterless constructor
    public Employee(){
        this("Robert Bobby",8000, "Park Point Drive, Rochester, NY", "10th May, 1982");
    }

    //main method
    public static void main(String[] args) {
        Employee employee1 = new Employee(); //calling the parameterless constructor
        Employee employee2 = new Employee("Jim Halpert",6000,
                "Scranton, Pennsylvania", "12th Jan, 1978"); //calling the constructor with new parameters


        System.out.println("<==========================================>");
        System.out.println("Getting the default values of employee 1 and employee 2 using accessors");
        System.out.println("<==========================================>");
        //printing the default field values
        System.out.println(employee1.getName());
        System.out.println(employee1.getSalary());
        System.out.println(employee1.getAddress());
        System.out.println(employee1.getDateOfBirth());

        System.out.println("\n \n");

        System.out.println(employee2.getName());
        System.out.println(employee2.getSalary());
        System.out.println(employee2.getAddress());
        System.out.println(employee2.getDateOfBirth());

        System.out.println("\n \n");


        //updating fields with new information using mutators
        employee1.setName("Robert California");
        employee1.setSalary(25000);
        employee1.setAddress("Agra, India");
        employee1.setDateOfBirth("26th January, 1965");

        System.out.println("\n \n");

        System.out.println("<==========================================>");
        System.out.println("After updating fields with new information");
        System.out.println("<==========================================>");
        //printing the object fields with new information
        System.out.println(employee1.getName());
        System.out.println(employee1.getSalary());
        System.out.println(employee1.getAddress());
        System.out.println(employee1.getDateOfBirth());
    }

}
