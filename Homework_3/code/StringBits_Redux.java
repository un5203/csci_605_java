/*
 * StringBits_Redux.java
 *
 *  Version 1.01
 */


/**
 * This class converts a given input string of any length into
 * bit format and vice versa.
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */


import java.util.Scanner;

public class StringBits_Redux {
    /**
     * The main program
     *
     * @param args  General arguments given to main function (ignored)
     *
     * @return void
     */
    public static void main(String[] args) {
        StringBits_Redux str = new StringBits_Redux();
        String word = "";
        while (!word.equalsIgnoreCase("quit")) {
            System.out.println("Enter any string: ");
            Scanner input = new Scanner(System.in);
            word = input.nextLine();
            if ("quit".equalsIgnoreCase(word)) {
                System.out.println("Thanks for playing !");
            } else {
                String wordToBits = str.strToByteString(word);
                String bitsToWord = str.byteStringToString(wordToBits);
                System.out.println("<===========================================>");
                System.out.println("original string: " + word);
                System.out.println("bits: " + wordToBits);
                System.out.println("final string: " + bitsToWord);
                System.out.println("<===========================================>");
            }
        }

    }

    /**
     *
     * @param ch    Accepts a single character
     * @return String  Returns a string of bits
     */
    //This function converts any single character to its 8 bit binary format
    public String charToByteString(char ch) {
        int temp = (int) ch;
        String str = "";
        for (int i = 7; i >= 0; i--) {
            str += String.valueOf((temp & (int) Math.pow(2, i)) >> i);
        }
//        String str = String.valueOf((temp & 128) >> 7);
//        str += String.valueOf((temp & 64) >> 6);
//        str += String.valueOf((temp & 32) >> 5);
//        str += String.valueOf((temp & 16) >> 4);
//        str += String.valueOf((temp & 8) >> 3);
//        str += String.valueOf((temp & 4) >> 2);
//        str += String.valueOf((temp & 2) >> 1);
//        str += String.valueOf((temp & 1) >> 0);
        return str;
    }

    //This function converts any input string into its binary format
    public String strToByteString(String str) {
        String temp = "";
        for (int i = 0; i < str.length(); i++) {
            temp += charToByteString(str.charAt(i));
        }
        return temp;
    }

    //This function converts a 8 bit binary format string into a character
    public String byteStringToChar(String str) {
        int num = 0;
        for (int i = 1; i < 8; i++) {
            num += (int) (Math.pow(2, 7 - i)) * Integer.parseInt(String.valueOf(str.charAt(i)));
        }

//        int i = 64 * Integer.parseInt(String.valueOf(str.charAt(1)));
//        i += 32 * Integer.parseInt(String.valueOf(str.charAt(2)));
//        i += 16 * Integer.parseInt(String.valueOf(str.charAt(3)));
//        i += 8 * Integer.parseInt(String.valueOf(str.charAt(4)));
//        i += 4 * Integer.parseInt(String.valueOf(str.charAt(5)));
//        i += 2 * Integer.parseInt(String.valueOf(str.charAt(6)));
//        i += 1 * Integer.parseInt(String.valueOf(str.charAt(7)));

        return String.valueOf((char) num);
    }

    //This function converts a binary format string into a string of characters
    public String byteStringToString(String str) {
        String temp = "";
        for (int i = 0; i < str.length(); i += 8) {
            temp += byteStringToChar(str.substring(i, i + 8));
        }
        return temp;
    }
}
