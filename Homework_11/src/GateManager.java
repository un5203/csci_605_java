/*
 * GateManager.java
 *
 * Version 1.0.1
 */

import java.util.LinkedList;

/**
 * This class manages the planes coming and leaving from gates
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */
public class GateManager implements Runnable {

    private LinkedList<Plane> planesInSky;
    private LinkedList<Plane> planesAtGates;

    /**
     * Constructor that references planes in the sky and planes at gates.
     */
    public GateManager(){
        this.planesInSky = Airport.getPlanesInSky();
        this.planesAtGates = Airport.getPlanesAtGates();
    }


    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        while(true){
            Plane plane ;
            synchronized (planesAtGates){
                while(planesAtGates.size() == 0){
                    try {
                        planesAtGates.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                plane = planesAtGates.removeFirst();
                RunwayManager.gatesOpen = RunwayManager.gatesOpen + 1 ;
                planesAtGates.notifyAll();
            }
            System.out.println("Plane " + Airport
                    .getPlaneName(plane) + " has arrived at " + Thread
                    .currentThread().getName() );
            System.out.println(plane.getAttendance() + " passengers are " +
                    "disembarking from plane " +
                    Airport
                    .getPlaneName(plane));
            try {
                Thread.currentThread().sleep(plane.getAttendance() * 200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("All passengers from plane " +
                    Airport
                    .getPlaneName(plane) + " have disembarked !");
            System.out.println("Plane " + Airport
                    .getPlaneName(plane) + " is taking off from " + Thread
                    .currentThread().getName());
        }
    }
}
