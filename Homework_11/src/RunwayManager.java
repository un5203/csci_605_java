/*
 * RunwayManager.java
 *
 * Version 1.0.1
 */

import java.util.LinkedList;

/**
 * This class manages the planes coming and leaving for the runways.
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */
public class RunwayManager implements Runnable{

    private LinkedList<Plane> planesInSky;
    private static LinkedList<Plane> planesAtGates;

    public static int gatesOpen = 10; //keeps track of open gates.

    /**
     * Constructor that references planes in the sky and planes at gates.
     */
    public RunwayManager(){
        this.planesInSky = Airport.getPlanesInSky();
        this.planesAtGates = Airport.getPlanesAtGates();
    }



    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        while(true){
            Plane plane;
            //get a hold of planesinsky and remove the first item
            synchronized (planesInSky){
                while(planesInSky.size() == 0){
                    try {
                        System.out.println(Thread.currentThread().getName() +
                                " is still empty");
                        planesInSky.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                plane = planesInSky.removeFirst();
                planesInSky.notifyAll();
            }
            System.out.println("Landing plane " + Airport.getPlaneName(plane)
                    + " at " + Thread.currentThread().getName());
            //after getting the first item, sleep for that amount of time
            // it takes for the plane to taxi
            try {
                Thread.sleep(plane.getTaxyingTime()*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (planesAtGates){
                while( gatesOpen == 0){
                    try {
                        System.out.println("RunwayManager: All gates are full" +
                                " !");
                        planesAtGates.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                planesAtGates.add(plane);
                gatesOpen = gatesOpen - 1;
                planesAtGates.notifyAll();
            }
        }
    }
}
