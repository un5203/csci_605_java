/**
 * ProduceBag.java
 * Version 1.0.1
 */
import java.util.ArrayList;
import java.util.Queue;

/**
 * This class allows a thread a produces a bag until the queue is full and it has the lock to it
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */
public class ProduceBag implements Runnable {
    private final Queue<Integer> queue;
    private ArrayList<Integer> bags;
    private int k;

    public ProduceBag(Queue<Integer> q, int s) {
        /**
         * Constructor that accpets the Bag Queue and the size of number of bags to store
         */
        this.queue = q;
        this.k = s;
        this.bags = new ArrayList<>(s);

    }

    @Override
    public void run() {
        /**
         * Function that the thread executes in Runnable state that calls the produceBags() function
         */
        for (int i = 1; i <= k; i++) {
            produceBags(i);
        }
    }

    private void produceBags(int i) {
        /**
         * Function that produces 5 bags at a time and then waits until a bag gets consumed
         * to fill more into the queue
         */
        synchronized (queue) {
            while (queue.size() == 5) {
                System.out.println("Bag Production Full... Only 5 Bags per queue");
                try {
                    queue.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Produced Bags #:" + i);
            queue.add(i);
            bags.add(i);
            queue.notify();
        }
    }
}
