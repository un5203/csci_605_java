/**
 * ConsumeBricks.java
 * Version 1.0.1
 */

import java.util.Queue;

/**
 * This class allows a thread a consume a brick if its in the queue and if it has the lock to it
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */
public class ConsumeBricks implements Runnable {
    private final Queue<Integer> queue;
    private Queue<Integer> bagqueue;
    private Queue<Integer> boxqueue;
    public int m;

    public ConsumeBricks(Queue<Integer> q1, Queue<Integer> q2, Queue<Integer> q3) {
        /**
         * Constructor that accpets the Brickqueue,BagQueue and BoxQueue
         */
        this.queue = q1;
        this.bagqueue = q2;
        this.boxqueue = q3;
        this.m = 7;
    }

    @Override
    public void run() {
        /**
         * The function that the thread executes when in Runnable state. Calls the consumeBricks() function
         * Once 12 bricks are consumed one plastic bag is consumed
         * If no more bags are available stop the production
         */
        while (true) {
            System.out.println("Consumed Brick #" + " " + consumeBricks());
            try {
                Thread.sleep(100);
                if (queue.isEmpty()) {
                    ConsumeBags c = new ConsumeBags(bagqueue, boxqueue, m);
                    ProduceBrick p = new ProduceBrick(queue, 20);
                    Thread t = new Thread(c);
                    Thread t2 = new Thread(p);
                    if (c.getBagQueue().isEmpty()) {
                        t.stop();
                        t2.stop();
                        System.out.print("No more Bags... Stop Production");
                        break;
                    } else {
                        t2.start();
                        t.start();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private int consumeBricks() {
        /**
         * THe function that consumes a brick if the thread obtains the lock on the queue
         * and consumes until queue is empty
         */
        synchronized (queue) {
            while (queue.isEmpty()) {
                try {
                    System.out.println("Brick Production Empty");
                    queue.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            int x = queue.remove();
            queue.notify();
            return x;
        }
    }
}
