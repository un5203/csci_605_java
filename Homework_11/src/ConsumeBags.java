/**
 * ConsumeBags.java
 * Version 1.0.1
 */

import java.util.ArrayList;
import java.util.Queue;

/**
 * This class allows a thread a consume a bag if its in the queue and it has the lock to it
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */
public class ConsumeBags implements Runnable {
    private final Queue<Integer> queue;
    private Queue<Integer> boxq;
    private ArrayList<Integer> bagged;
    private int m;
    public static int x;

    public ConsumeBags(Queue<Integer> q1, Queue<Integer> q2, int size) {
        /**
         * Constructor that takes in the Bagqueue , BoxQueue and the size for storing the bagged bricks
         */
        this.queue = q1;
        this.boxq = q2;
        m = size;
        this.bagged = new ArrayList<>(m);
    }

    public static synchronized void incrementcount() {
        /**
         * A static function that keeps track of the number of bagged bricks
         */
        x++;
    }

    @Override
    public void run() {
        /**
         * The function that the thread executes in Runnable state that consumes a bag by calling the consumeBags() function
         */
        int a = consumeBags();
        incrementcount();
        bagged.add(a);
        System.out.println("\t" + "Consumed Bag#" + a + " " + "THat contains 12 Toy Bricks");
        try {
            Thread.sleep(100);
            if (queue.isEmpty()) {
                ProduceBox p = new ProduceBox(boxq, m);
                Thread t1 = new Thread(p);
                t1.start();
                while (!bagged.isEmpty()) {
                    if (x >= 6) {
                        ConsumeBox c = new ConsumeBox(boxq, 6);
                        Thread t2 = new Thread(c);
                        x = x - 6;
                        t2.start();
                    } else {
                        ConsumeBox c = new ConsumeBox(boxq, x);
                        Thread t2 = new Thread(c);
                        t2.start();
                        bagged.clear();
                    }
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private int consumeBags() {
        /**
         * This function consumes a Bag if the queue is not empty and the 12 bricks have been consumed
         */
        synchronized (queue) {
            while (queue.isEmpty()) {
                System.out.println("Bag Production Empty");
                try {
                    queue.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            int x = queue.remove();
            queue.notify();
            return x;
        }
    }

    public Queue<Integer> getBagQueue() {
        /**
         * Returns the BagQueue to notify other machnes whether production has to be stopped
         * in case it is empty
         */
        return this.queue;
    }

}
