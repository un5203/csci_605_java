import java.util.ArrayList;
import java.util.Queue;

/**
 * This class allows a thread a consume a bag if its in the queue and it has the lock to it
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */

public class ProduceBox implements Runnable {
    private final Queue<Integer> queue;
    private ArrayList<Integer> boxes;
    private int m;

    public ProduceBox(Queue<Integer> q, int s) {
        /**
         * Constructor that accepts the Boxqueue and the number of boxes to create
         */
        this.queue = q;
        this.m = s;
        this.boxes = new ArrayList<>(s);

    }

    @Override
    public void run() {
        /**
         * The function that thread executes in Runnable state and calls the produceBoxes() function
         */
        for (int i = 1; i <= m; i++)
            produceBoxes(i);
    }

    private void produceBoxes(int i) {
        /**
         * Function the produces boxes based on the number of Bagged Bricks
         */
        synchronized (queue) {
            while (queue.size() == 6) {
                System.out.println("Boxes Production Full");
                try {
                    queue.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Produced Boxes #:" + i);
            queue.add(i);
            boxes.add(i);
            queue.notify();
        }
    }
}
