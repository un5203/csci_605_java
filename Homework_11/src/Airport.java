/*
 * Airport.java
 *
 * Version 1.0.1
 */



import java.util.LinkedList;


/**
 * This is a helper class to TrafficControl system that encapsulates details
 * about an airport.
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */
public class Airport implements Runnable{

    private static final LinkedList<Plane> planesInSky = new LinkedList<Plane>();

    private static final LinkedList<Plane> planesAtGates = new LinkedList<Plane>();


    public static LinkedList<Plane> getPlanesInSky() {
        return planesInSky;
    }

    public static LinkedList<Plane> getPlanesAtGates() {
        return planesAtGates;
    }

    public static String getPlaneName(Plane plane){
        return new String(plane.getId().toString
                ()).substring(0,10);
    }



    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {

    }

    /**
     * Generates a random plane and puts it in the sky
     */
    public static void addPlaneAndWait(){
        synchronized (planesInSky) {
            planesInSky.add(new Plane());
//            planesInSky.add(new Plane());
//            planesInSky.add(new Plane());
//            planesInSky.add(new Plane());
//            planesInSky.add(new Plane());
//            planesInSky.add(new Plane());
//            planesInSky.add(new Plane());
//            planesInSky.add(new Plane());
//            planesInSky.add(new Plane());
//            planesInSky.add(new Plane());
//            planesInSky.add(new Plane());
            planesInSky.notifyAll();
        }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

    }

    /**
     * Prints all the planes that are currently flying
     * @param planes
     */
    public static void flyingPlanes(LinkedList<Plane> planes){
        System.out.println("##################################");
        for (Plane plane :planes) {
            System.out.println("Plane "+ new String(plane.getId().toString
                    ()).substring(0,10) +
                    " " +
                    "is still flying !");
        }
        System.out.println("##################################");
    }

    /**
     * Main function that creates threads to manage the flights on runway and
     * at gates and also generates random planes every 5 seconds. This
     * function demonstrates how an air traffic control works.
     * @param args
     */
    public static void main(String[] args) {
        Airport airport = new Airport();
        RunwayManager runwayManager = new RunwayManager();
        GateManager gateManager = new GateManager();
        Thread[] runways = new Thread[4];
        Thread[] gates = new Thread[10];
        for(int i = 0;i<runways.length;i++){
            runways[i] = new Thread(runwayManager);
            runways[i].setName("Runway # " + (i+1));
            runways[i].start();
        }

        for(int i = 0; i < gates.length ; i++){
            gates[i] = new Thread(gateManager);
            gates[i].setName("Gate # " + (i+1));
            gates[i].start();
        }

        while(true) {
            addPlaneAndWait();
            flyingPlanes(planesInSky);
        }
    }
}


