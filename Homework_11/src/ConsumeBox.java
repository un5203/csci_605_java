/**
 * ConsumeBox.java
 * Version 1.0.1
 */

import java.util.Queue;

/**
 * This class allows a thread a consume a box if its in the queue and it has the lock to it
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */
public class ConsumeBox implements Runnable {
    private final Queue<Integer> queue;
    private int count;

    public ConsumeBox(Queue<Integer> q, int numbags) {
        /**
         * Constructor that accepts a BoxQueue and
         * the number of boxes to be produced
         */
        this.count = numbags;
        this.queue = q;
    }

    @Override
    public void run() {
        /**
         * The function that the thread executes in Runnable state. Calls the consumeBox() function
         */
        System.out.println("\t" + "Consumed Box # " + consumeBox() + " " + "That contains" + " " + count + " " + "Bags");
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private int consumeBox() {
        /**
         * Consumes a box that stores at most 6 bags of bricks until the queue is empty
         */
        synchronized (queue) {
            while (queue.isEmpty()) {
                System.out.println("Box Production Empty" + " " + "Size is now " + queue.size());
                try {
                    queue.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            int x = queue.remove();
            queue.notify();
            return x;
        }
    }
}
