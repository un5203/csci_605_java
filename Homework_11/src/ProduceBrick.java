/**
 * ProduceBrick.java
 * <p/>
 * Version 1.0.1
 */

import java.util.ArrayList;
import java.util.Queue;

/**
 * This class allows a thread to produce a brick until the queue is full and it has the lock to it
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */
public class ProduceBrick implements Runnable {
    private final Queue<Integer> queue;
    private ArrayList<Integer> bricks;

    public ProduceBrick(Queue<Integer> q, int s) {
        /**
         * Constructor that accepts the Brick queue and the size of number of bricks to store
         */
        this.queue = q;
        this.bricks = new ArrayList<>(s);

    }

    @Override
    public void run() {
        /**
         * Function that the thread executes in RUnnable state that calls produceBricks() function
         */
        for (int i = 1; i <= 12; i++) {
            produceBricks(i);
        }
    }

    private void produceBricks(int i) {
        /**
         * Function that produces 10 bricks at a time and waits
         * until a brick gets consumed to add more to the queue
         */
        synchronized (queue) {
            while (queue.size() == 10) {
                try {
                    System.out.println("Brick Production Full... Only 10 Bricks can be in queue");
                    queue.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Produced Brick #:" + i);
            queue.add(i);
            bricks.add(i);
            queue.notify();
        }
    }
}
