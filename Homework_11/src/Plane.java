/*
 * Plane.java
 *
 * Version 1.0.1
 */


import java.util.Random;
import java.util.UUID;


/**
 * This is a helper class to TrafficControl system that encapsulates details
 * about a plane.
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */

public class Plane implements Runnable{

    private UUID id = UUID.randomUUID();
    private int max_capacity; //maximum capacity of the plane
    private int attendance; // number of passengers in the plane
    private String size; // size of the plane : small, medium or large.

    public int getTaxyingTime() {
        return taxyingTime;
    }

    public void setTaxyingTime(int taxyingTime) {
        this.taxyingTime = taxyingTime;
    }

    private int taxyingTime; // time taken by a type of a plane to taxi on
    // runway.
    private static final String[] randomSize = {
            "small","medium", "large"}; // array that holds random size
    private static Random rand = new Random(); // class that generates random
    // numbers.


    /**
     * returns the unique id of a plane.
     * @return
     */
    public UUID getId() {
        return id;
    }

    /**
     * gets the number of passengers in the plane
     * @return
     */
    public int getAttendance() {
        return attendance;
    }

    /**
     * sets the number of passengers in the plane
     * @param attendance
     */
    public void setAttendance(int attendance) {
        this.attendance = attendance;
    }

    /**
     * gets the maximum possible capacity of the plane
     * @return
     */
    public int getMax_passengers() {
        return max_capacity;
    }

    /**
     * gets the size of the plane. Can be "small", "medium" or "large"
     * @return
     */
    public String getSize() {
        return size;
    }

    /**
     * generates a random number of possible passengers for a given plane.
     * @param max_capacity
     * @return
     */
    public int generateAttendance(int max_capacity){
        int upperbound = max_capacity;
        int lowerbound = (int) (upperbound * 0.80);
        return  (rand.nextInt(upperbound - lowerbound) + lowerbound);
    }

    /**
     * sets the maximum number of passengers of a given plane.
     * @param max_passengers
     */
    private void setMax_passengers(int max_passengers) {
        this.max_capacity = max_passengers;
    }

    /**
     * Constructs different kinds of planes depending on given size.
     * @param size
     */
    public Plane(String size){
        if(size == "small"){
            setMax_passengers(10);
            setTaxyingTime(8);
            this.size = size;
            setAttendance(generateAttendance(max_capacity));
        }
        else if(size == "medium"){
            setMax_passengers(50);
            setTaxyingTime(6);
            this.size = size;
            setAttendance(generateAttendance(max_capacity));
        }
        else if(size == "large"){
            setMax_passengers(100);
            setTaxyingTime(4);
            this.size = size;
            setAttendance(generateAttendance(max_capacity));
        }
    }

    /**
     * Parameterless constructor. Will generate a random plane with a random
     * size, max_capacity and attendance.
     */
    public Plane(){
        this(randomSize[rand.nextInt(3)]);
    }

    /**
     * main function for testing plane attributes.
     * @param args
     */
    public static void main(String[] args) {
        Plane[] planes = new Plane[5];
        for(int i = 0 ; i < planes.length; i++){
            planes[i] = new Plane();
            System.out.println("*********************************************");
            System.out.println("Plane "+i+ ":");
            System.out.println("ID : "+planes[i].getId());
            System.out.println("Size : "+planes[i].getSize());
            System.out.println("Time spent taxiing : "+planes[i].getTaxyingTime());
            System.out.println("Max Capacity : "+ planes[i].getMax_passengers());
            System.out.println("No of passengers : "+planes[i].getAttendance());
        }
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        System.out.println("Plane is arriving");
    }
}
