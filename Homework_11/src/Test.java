/**
 * Test.java
 * Version 1.0.1
 */

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) throws InterruptedException {
        /**
         * Main function to test The Toy brick factory production and storage
         */
//        int k = args[0];
//        int l = args[1];
        int k = 12;
        int l = 7;
        Queue<Integer> brickqueue = new LinkedList<>();
        Queue<Integer> bagqueue = new LinkedList<>();
        Queue<Integer> boxqueue = new LinkedList<>();
        ProduceBrick produceBrick = new ProduceBrick(brickqueue, k);
        ProduceBag produceBag = new ProduceBag(bagqueue, l);
        ConsumeBricks consumeBricks = new ConsumeBricks(brickqueue, bagqueue, boxqueue);
        Thread t1 = new Thread(produceBrick);
        Thread t2 = new Thread(produceBag);
        Thread t4 = new Thread(consumeBricks);
        t1.start();
        t2.start();
        t4.start();
    }
}
