import java.net.*;
import java.io.*;
import java.util.Scanner;

public class ChatClient{
    public static void main(String[] args) throws IOException {
        String host = args[0];
        String port = args[1];
        Socket sock = new Socket(host, Integer.parseInt(port));
        OutputStream out = sock.getOutputStream();
        InputStream in = sock.getInputStream();
        System.out.println("Please enter your username:");
        Scanner typer = new Scanner(System.in);
        String msg = typer.nextLine();
        out.write(msg.getBytes());
        out.flush();
        byte[] buffer = new byte[102400];
        int n;
        while(true) {
            n = in.read(buffer);
            String message = new String(buffer, 0, n);
            System.out.println(message);
            if(message.equals("Bye Bye !!")){
                break;
            }
            else if(message.equals("")){
                out.write("".getBytes());
                out.flush();
            }
            if((msg = typer.nextLine())!=""){
                out.write(msg.getBytes());
                out.flush();
            }
        }
    }
}
