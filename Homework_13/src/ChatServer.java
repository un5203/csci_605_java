import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Map;

public class ChatServer implements Runnable {

    private static final Object lock = new Object();
    private HashMap<String,Socket> connectedUsers = new
            HashMap<>();
    private ServerSocket server;

    public ChatServer(String port) throws IOException{
        server = new ServerSocket(Integer.parseInt(port));
    }

    public static void main(String[] args) throws IOException {
        new Thread(new ChatServer(args[0])).start();
    }

    public void getConnectedUsers(){
        for(String key : connectedUsers.keySet()){
            System.out.println("- ["+key+"]\n");
        }
    }


    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        while(true) {
            try {
                getConnectedUsers();
                System.out.println("waiting for connections...");
                Socket client = server.accept();
                System.out.println("connection estalished!");
                new Thread(new ClientHandler(client,connectedUsers)).start();
            }
            catch (IOException e) {
            }
        }
    }
}
