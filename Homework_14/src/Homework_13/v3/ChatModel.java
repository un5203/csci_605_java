package Homework_13.v3; /**
 * ChatModel.java
 * Version 1.0.1
 */

import java.io.*;
import java.net.Socket;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Implements the application logic that acts as the backend for messages.
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */
public class ChatModel implements Runnable{

    private String currentUser = ""; //name of the current user
    private Observer observer; //observer pattern
    private Socket sock;
    private String chatMessages = null;
    private static final Object lock = new Object();
    private boolean waiting = true;



    /**
     * Constructor
     */
    public ChatModel(Socket sock) throws IOException {
        this.sock = sock;
        new Thread(this).start();
    }


    public Map<String,String> message(){
        Map<String,String> data = new HashMap<>();
        return data;
    }

    /**
     * Keeps track of observers.
     * @param observer
     */
    public void attachObserver(Observer observer){
        this.observer = observer;
    }


    /**
     * Notifies the observer with a generic or error message
     * @param message - message to pass
     * @param flag - "error" | "general" (type of message)
     */
    public void notifyObserver(String message,String flag){
        observer.update(message,flag);
    }

    /**
     * Notify observers that messages have been sent to them.
     */
    public void checkMessages(){
        observer.youGotMail();
    }

    /**
     * When a new user joins, his/her name is converted to a file and added
     * here.
     */
    public void addUser(String username) throws IOException, ChatException, ClassNotFoundException {
        Map<String,String> data = message();
        data.put("command","addUser");
        data.put("username",username);
        OutputStream out = sock.getOutputStream();
        ObjectOutputStream oout = new ObjectOutputStream(out);
        oout.writeObject(data);
        oout.flush();

        InputStream in = sock.getInputStream();
        ObjectInputStream oin = new ObjectInputStream(in);
        Map<String,String> response = (Map<String,String>) oin.readObject();
        if(!response.get("response").equals("ok")){
            throw new ChatException("User already exists");
        }

        currentUser = username;
    }


    /**
     * Display all the online users
     * @return
     */
    public String showOnlineUsers() throws IOException, ClassNotFoundException {
        OutputStream out = sock.getOutputStream();
        ObjectOutputStream oout = new ObjectOutputStream(out);
        Map<String,String> data = message();
        data.put("command","getOnlineUsers");
        oout.writeObject(data);
        oout.flush();
        synchronized (lock) {
            waiting = true;
            notify();
        }
        InputStream in = sock.getInputStream();
        ObjectInputStream oin = new ObjectInputStream(in);
        Map<String,String> response = (Map<String,String>) oin.readObject();
        synchronized (lock){
            waiting = false;
            notify();
        }
//        if(response.containsKey("chatMessages")){
//            chatMessages += response.get("chatMessages");
//            observer.youGotMail();
//        }
        System.out.println(response);

        if(response.containsKey("onlineUsers")){
            return String.valueOf(response.get("onlineUsers"));
        }
        else{
            return null;
        }
    }

    /**
     * sends a message to a user
     * @param message
     * @param toUsername
     * @throws IOException
     */
    public void sendMessageTo(String message, String toUsername) throws ChatException, IOException, ClassNotFoundException {
        if(toUsername.equals(currentUser)){
            throw new ChatException("Cannot send a message to yourself!");
        }
        else{
            String toWrite =  "(" + Calendar.getInstance()
                    .getTime() +
                    ") - " + currentUser+ " : " +
                    message + "\n";
            Map<String,String> data = message();
            data.put("command","sendMessage");
            data.put("to",toUsername);
            data.put("message",toWrite);
            OutputStream out = sock.getOutputStream();
            ObjectOutputStream oout = new ObjectOutputStream(out);
            oout.writeObject(data);
            oout.flush();
            synchronized (lock) {
                waiting = true;
                notify();
            }
            if(sock.getInputStream().available()!=0){
                InputStream in = sock.getInputStream();
                ObjectInputStream oin = new ObjectInputStream(in);
                Map<String,String>response = (Map<String,String>) oin.readObject();
//                if(response.containsKey("chatMessages")){
//                    chatMessages += response.get("chatMessages");
//                    observer.youGotMail();
//                }
                System.out.println(response);
                if(!response.get("response").equals("ok")){
                    throw new ChatException("User "+ toUsername + " is not " +
                            "online!");
                }
                synchronized (lock) {
                    waiting = true;
                    notify();
                }
            }
        }
    }

    /**
     * Removes the user files and quits
     */
    public void quit() throws IOException, ClassNotFoundException {
        Map<String,String> data = message();
        data.put("command","deleteUser");
        data.put("username",currentUser);
        OutputStream out = sock.getOutputStream();
        ObjectOutputStream oout = new ObjectOutputStream(out);
        oout.writeObject(data);
        oout.flush();
        currentUser = "";
    }

    /**
     * Returns messages of a particular user
     * @return
     * @throws IOException
     */
    public String getMessages() throws IOException, ClassNotFoundException {
        String temp = chatMessages;
        chatMessages = null;
        return temp;
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        while(true){
            synchronized (lock){
                if(waiting){
                    try {
                        wait();
                        notify();
                    } catch (InterruptedException e) {

                    }
                }
                else{
                    InputStream in = null;
                    ObjectInputStream oin = null;
                    Map<String,String> response = null;
                    try {
                        in = sock.getInputStream();
                        oin = new ObjectInputStream(in);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        response = (Map<String,String>) oin.readObject();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    if(response.containsKey("chatmessage")){
                        chatMessages = response.get("chatmessage");
                        observer.youGotMail();
                    }
                }
            }
        }
    }
}
