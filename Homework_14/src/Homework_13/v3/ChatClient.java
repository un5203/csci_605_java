package Homework_13.v3;

import java.io.IOException;
import java.net.Socket;

/**
 * Created by Ujwal on 06/05/2016.
 */
public class ChatClient {
    public static void main(String[] args) throws Exception {
        Socket sock = null;
        try {
            sock = new Socket("localhost", Integer.parseInt("12345"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Starting the model!");
        ChatModel model = new ChatModel(sock);
        System.out.println("Starting the controller!");
        ChatController controller = new ChatController(model);
        ChatWindow view = new ChatWindow(controller);
        System.out.println("Starting the view!");
        view.start();
        System.out.println("The end!");
    }
}
