package Homework_13.v3;

import java.io.*;
import java.net.Socket;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ujwal on 06/05/2016.
 */
public class ClientHandler implements Runnable{
    private Map<String,Socket> onlineUsers;
    private Socket sock;
    private String currentUser;
    private String chatMessages = null;

    public ClientHandler(Map<String, Socket> onlineUsers, Socket sock) throws IOException {
        this.onlineUsers = onlineUsers;
        this.sock = sock;
    }




    public Map<String,String> message(){
        Map<String,String> data = new HashMap<>();
        return data;
    }

    public void MainStuff() throws IOException, ClassNotFoundException{
        boolean stop = false;
        while(!stop){
            InputStream in = sock.getInputStream();
            ObjectInputStream oin = null;
            try{oin = new ObjectInputStream(in);}
            catch(EOFException eof){}
            Map<String,String> data = (Map<String,String>) oin.readObject();
            System.out.println(data);
            String command = (String) data.get("command");
            switch (command){
                case "addUser":
                    addUser((String) data.get("username"));
                    break;
                case "sendMessage":
                    sendMessageTo(data.get("message"),data.get("to"));
                    break;
                case "youGotMail":
                    getMessage(data.get("message"));
                    break;
                case "getOnlineUsers":
                    getOnlineUsers();
                    break;
                case "deleteUser":
                    onlineUsers.remove(currentUser);
                    System.out.println(currentUser+" left the chat!");
                    stop = true;
                    break;
            }
        }
    }

    private void getMessage(String message) throws IOException {
        OutputStream out = sock.getOutputStream();
        ObjectOutputStream oout = new ObjectOutputStream(out);
        Map<String,String> response = message();
        response.put("chatmessage",message);
        oout.writeObject(response);
        oout.flush();
    }


    /**
     * sends a message to a user
     * @param message
     * @param toUsername
     * @throws IOException
     */
    public void sendMessageTo(String message, String toUsername) throws IOException, ClassNotFoundException {
        if(onlineUsers.containsKey(toUsername)){
            Socket toSock = onlineUsers.get(toUsername);
            OutputStream out = toSock.getOutputStream();
            Map<String,String> data = message();
            data.put("command","youGotMail");
            data.put("message",message);
//            if(chatMessages!=null){
//             data.put("chatMessages",chatMessages);
//                chatMessages = null;
//            }
            data.put("response","ok");
            ObjectOutputStream oout = new ObjectOutputStream(out);
            oout.writeObject(data);
            oout.flush();
        }
        else{
            Map<String,String> response = message();
            response.put("response","notOK");
//            if(chatMessages!=null){
//                response.put("chatMessages",chatMessages);
//                chatMessages = null;
//            }
            OutputStream out2 = sock.getOutputStream();
            ObjectOutputStream oout2 = new ObjectOutputStream(out2);
            oout2.writeObject(response);
            oout2.flush();
        }
    }

    private void getOnlineUsers() throws IOException {
        Map<String,String> response = message();
        OutputStream out = sock.getOutputStream();
        ObjectOutputStream oout = new ObjectOutputStream(out);
        String users = "";
        for(String user : onlineUsers.keySet()){
            users += "[" +user+"] ";
        }
        response.put("onlineUsers",users);
//        if(chatMessages!=null){
//            response.put("chatMessages",chatMessages);
//            chatMessages = null;
//        }
        oout.writeObject(response);
        oout.flush();
    }

    private boolean isUserOnline(String username) throws IOException {
        return onlineUsers.containsKey(username);
    }

    private void addUser(String username) throws IOException {
        if(isUserOnline(username)){
            System.out.println("User " +username + " already exists!");
            Map<String,String> response = message();
            OutputStream out = sock.getOutputStream();
            ObjectOutputStream oout = new ObjectOutputStream(out);
            response.put("response","notOK");
//            if(chatMessages!=null){
//                response.put("chatMessages",chatMessages);
//                chatMessages = null;
//            }
            oout.writeObject(response);
            oout.flush();
        }
        else{
            currentUser = username;
            System.out.println("Added "+username+" to the database!");
            onlineUsers.put(currentUser, sock);
            Map<String,String> response = message();
            OutputStream out = sock.getOutputStream();
            ObjectOutputStream oout = new ObjectOutputStream(out);
            response.put("response","ok");
//            if(chatMessages!=null){
//                response.put("chatMessages",chatMessages);
//                chatMessages = null;
//            }
            oout.writeObject(response);
            oout.flush();
        }
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
            try {
                MainStuff();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
    }

}
