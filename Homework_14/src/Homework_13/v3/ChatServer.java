package Homework_13.v3;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ujwal on 06/05/2016.
 */
public class ChatServer{

    private ServerSocket server;
    private Map<String,Socket> onlineUsers;

    public ChatServer(String port) {
        try {
            server = new ServerSocket(Integer.parseInt(port));
            onlineUsers = new HashMap<>();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ChatServer server = new ChatServer("12345");
        while(true){
            System.out.println("Waiting for connections...");
            try {
                Socket sock = server.server.accept();
                new Thread(new ClientHandler(server.onlineUsers,sock)).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Connection has been established !");
        }
    }
}
