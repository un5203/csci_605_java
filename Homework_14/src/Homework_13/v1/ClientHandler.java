package Homework_13.v1;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.HashMap;

/**
 * Created by Ujwal on 01/05/2016.
 */
public class ClientHandler implements Runnable {
    private Socket sock;
    private HashMap<String,Socket> connectedUsers;

    public ClientHandler(Socket sock, HashMap<String, Socket> connectedUsers){
        this.sock = sock;
        this.connectedUsers = connectedUsers;
    }

    public void getConnectedUsers() throws IOException {
        String temp = "";
        for(String key : connectedUsers.keySet()){
            temp += "- ["+key+"]\n";
        }
        sendMessage(temp);
    }

    public void sendMessage(String msg) throws IOException {
        OutputStream out = sock.getOutputStream();
        out.write(msg.getBytes());
    }

    public void checkUsername(String username) throws IOException {
        if(connectedUsers.containsKey(username)){
            sendMessage("User already exists");
        }
        else{
            connectedUsers.put(username,sock);
        }
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        try {
            InputStream in = sock.getInputStream();
            OutputStream out = sock.getOutputStream();
            byte[] buffer = new byte[102400];
            int n;
            String username = null;
            if((n = in.read(buffer)) != -1) {
                username = new String(buffer, 0, n);
                checkUsername(username);
            }

            boolean stop = false;

                String message = "Select an option from the menu:\n" +
                        "L - List all online users\n" +
                        "M - Message a particular user\n" +
                        "Q - Quit";
                out.write(message.getBytes());
            while(!stop) {
                String option = null;
                if ((n = in.read(buffer)) != -1) {
                    option = new String(buffer, 0, n);
                }
                switch (option) {
                    case "l": //list all users
                        getConnectedUsers();
                        out.write(message.getBytes());
                        break;
                    case "m": //show the list of users and find who the message
                        // is for.
                        getConnectedUsers();
                        String name = "";
                        if ((n = in.read(buffer)) != -1) {
                            name = new String(buffer, 0, n);
                        }
                        System.out.println(name + " is being sent a message");
                        if(!connectedUsers.containsKey(name)){
                            sendMessage("User does not exist !");
                        }
                        else{
                            sendMessage("Enter your message");
                            String userMessage = "";
                            if ((n = in.read(buffer)) != -1) {
                                userMessage = new String(buffer, 0, n);
                            }
                            Socket to = connectedUsers.get(name);
                            to.getOutputStream().write(userMessage.getBytes());
                            sendMessage("Your message has been sent !");
                            out.write(message.getBytes());
                        }
                        break;
                    case "q": // quit the program.
                        synchronized (connectedUsers) {
                            connectedUsers.remove(username);
                        }
                        sendMessage("Bye Bye !!");
                        sock.close();
                        stop = true;
                        break;
                }

            }
        }
        catch(Exception e) {

        }
    }
}
