/**
 * Server.java
 * Version 1.0.1
 */

package Homework_13.v2;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * Starts a chat server and creates a separate thread for each server.
 *
 * @author Ujwal Bharat Nagumantri
 * @author Yeshwanth Raja
 */
public class Server{

    //List of online users.
    private Map<String,Socket> onlineUsers;

    /**
     * Constructor
     */
    public Server(){
        this.onlineUsers = new HashMap<>();
    }

    /**
     * Main method
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        Server server = new Server();
        ServerSocket socket = new ServerSocket(12345);
        while(true){
            System.out.println("waiting for connections!");
            Socket sock = socket.accept();
            System.out.println("a client has connected!");
            //creates a new thread for the client.
            new Thread(new ChatClientHandler(sock, server.onlineUsers)).start();

        }
    }
}
