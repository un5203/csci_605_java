/**
 * Client.java
 *
 * Version 1.0.1
 */
package Homework_13.v2;


import java.io.*;
import java.net.Socket;
import java.util.Calendar;
import java.util.Scanner;
import java.util.logging.SocketHandler;

/**
 * Creates a client that connects to the chat server.
 *
 * @author Ujwal Bharat Nagumantri
 * @author Yeshwanth Raja
 */
public class Client implements Runnable{

    private Socket sock; //socket that connects this user to the server
    private String currentUser; // name of the current user

    /**
     * Constructor.
     * @param sock
     */
    public Client(Socket sock){
        this.sock = sock;
    }

    /**
     * Sends a message to the server.
     * @param message
     * @throws IOException
     */
    public void writeMessage(String message) throws IOException {
        OutputStream out = sock.getOutputStream();
        DataOutputStream dout = new DataOutputStream(out);
        dout.writeUTF(message);
    }

    /**
     * Reads a message from the server.
     * @return
     * @throws IOException
     */
    public String readMessage() throws IOException {
        InputStream in = sock.getInputStream();
        DataInputStream din = new DataInputStream(in);
        return din.readUTF();
    }

    /**
     * Sends a message to another user.
     * @param sock
     * @param message
     * @throws IOException
     */
    public void writeMessageTo(Socket sock, String message) throws IOException {
        OutputStream out = sock.getOutputStream();
        DataOutputStream dout = new DataOutputStream(out);
        dout.writeUTF(message);
    }

    /**
     * Displays a message
     * @param message
     */
    public String generateMessage(String message,String fromUser){
        String temp2 =  "(" + Calendar.getInstance()
                .getTime() +
                ") - " + fromUser+ " : " +
                message + "\n";
        String temp1 = printLooper("#",temp2.length());
        String temp3 = printLooper("#",temp2.length());
        return "\n"+temp1+temp2+temp3;
    }

    /**
     * Prints a character a certain number of times
     * @param character
     * @param noOfTimes
     */
    public String printLooper(String character, int noOfTimes){
        String temp = "";
        for(int i=0;i<noOfTimes;i++){
            temp += character;
        }
        temp += "\n";
        return temp;
    }

    /**
     * Main function.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        Socket sock = new Socket("localhost",12345); // connects to the server.
        Client client = new Client(sock); //creates a new client
        Scanner input = new Scanner(System.in); //gets the username from user
        while(true){
            //if username exists, retry.
            System.out.println("Please enter your username:");
            String username = input.nextLine();
            client.currentUser = username;
            client.writeMessage(username);
            String response = client.readMessage();
            if(response.equals("notOk")){
                System.out.println(client.readMessage());
            }
            else{
                System.out.println("connected to server !");
                break;
            }
        }
        boolean go = true;
        System.out.println("\nPlease select one of the options:");
        System.out.println("l - list of users");
        System.out.println("m - send message to another user");
        System.out.println("q - quit");
        Thread thread = new Thread(client);
        thread.start();
        while(go){
            //synch here
            String option = input.nextLine();
            client.writeMessage(option);
            if(option.equals("m")){
                System.out.println("Enter the name of the user to " +
                        "send a message to:");
                String toUser = input.nextLine();
                System.out.println("Enter your message");
                String toMessage = input.nextLine();
                client.writeMessage(toUser);
                toMessage = client.generateMessage(toMessage,client.currentUser);
                client.writeMessage(toMessage);
            }
            if(option.equals("q")){
                client.sock.close();
                System.out.println("Bye Bye!");
                go = false;
                System.exit(0);
            }
        }
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        while(true){
            try {
                String serverResponse = readMessage();
                System.out.println(serverResponse);
                System.out.println("\nPlease select one of the options:");
                System.out.println("l - list of users");
                System.out.println("m - send message to another user");
                System.out.println("q - quit");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
