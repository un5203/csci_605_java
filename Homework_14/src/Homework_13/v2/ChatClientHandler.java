package Homework_13.v2;
/**
 * ChatClientHandler.java
 * Version 1.01
 */

import java.io.*;
import java.net.Socket;
import java.util.Calendar;
import java.util.Map;

/**
 * Handles one unique user and enables him/her to message other users.
 *
 * @author Ujwal Bharat Nagumantri
 * @author Yeshwanth Raja
 */
public class ChatClientHandler implements Runnable{

    private Socket sock; //connection between one user and server.
    private Map<String,Socket> onlineUsers; //list of all online users.
    private String currentUser; //name of the current user.

    /**
     * Constructor
     * @param sock
     * @param onlineUsers
     */
    public ChatClientHandler(Socket sock, Map<String, Socket> onlineUsers) {
        this.sock = sock;
        this.onlineUsers = onlineUsers;
    }

    /**
     * Writes a message to connected user.
     * @param message : message to the connected user.
     * @throws IOException
     */
    public void writeMessage(String message) throws IOException {
        OutputStream out = sock.getOutputStream();
        DataOutputStream dout = new DataOutputStream(out);
        dout.writeUTF(message);
    }

    /**
     * Forwards a message from currentUser to another user.
     * @param sock : connection to another user.
     * @param message : message to another user.
     * @throws IOException
     */
    public void writeMessageTo(Socket sock,String message) throws IOException {
        OutputStream out = sock.getOutputStream();
        DataOutputStream dout = new DataOutputStream(out);
        dout.writeUTF(message);
    }

    /**
     * Receives a message from the currentUser.
     * @return
     */
    public String readMessage() {
        String received = "q";
        try {
            InputStream in = sock.getInputStream();
            DataInputStream din = new DataInputStream(in);
            received = din.readUTF();
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        return received;
    }

    /**
     * Gets list of online users.
     * @return
     */
    public String getOnlineUsers(){
        String message = "";
        for(String user : onlineUsers.keySet()){
            message += "[" +user+"] ";
        }
        return message;
    }

    /**
     * Makes the user's message prettier.
     * @param message
     * @param fromUser
     * @return
     */
    public String generateMessage(String message,String fromUser){
        String temp2 =  "(" + Calendar.getInstance()
                .getTime() +
                ") - " + fromUser+ " : " +
                message + "\n";
        String temp1 = printLooper("#",temp2.length());
        return "\n"+temp1+temp2+temp1;
    }

    /**
     * Prints a character a certain number of times
     * @param character
     * @param noOfTimes
     */
    public String printLooper(String character, int noOfTimes){
        String temp = "";
        for(int i=0;i<noOfTimes;i++){
            temp += character;
        }
        temp += "\n";
        return temp;
    }

    /**
     * Creates an error message.
     * @param message
     * @return
     */
    public String generateError(String message){
        String temp1 = printLooper("!",message.length());
        return "\n"+temp1+message+"\n"+temp1;
    }


    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        try {
            while(true) {
                //First time interaction with user.
                //Here the user's login username is received and checked for
                // uniqueness.
                String username = readMessage();
                if (onlineUsers.containsKey(username)) {
                    writeMessage("notOk");
                    writeMessage(generateError("User already exists!"));
                } else {
                    onlineUsers.put(username, sock);
                    currentUser = username;
                    writeMessage("ok");
                    System.out.println(username + " has connected !");
                    break;
                }
            }
            boolean go = true;
            while(go){
                //Here input is received from user and an appropriate action
                // is performed.
                String option = readMessage();
                switch(option){
                    case "l":
                        //returns a list of users
                        writeMessage(getOnlineUsers());
                        break;
                    case "m":
                        //sends a message to another user.
                        String toUser = readMessage();
                        String toMessage = readMessage();
                        if(onlineUsers.containsKey(toUser) && !toUser.equals
                                (currentUser)){
                            writeMessageTo(onlineUsers.get(toUser),toMessage);
                            writeMessage("\nYour message was successfully " +
                                    "sent!");
                        }
                        else if(currentUser.equals(toUser)){
                            writeMessage(generateError("You cannot send a " +
                                    "message to yourself!"));
                        }
                        else{
                            writeMessage(generateError("Unable to send a message to "
                                    +toUser+
                                    "!!"));
                        }
                        break;
                    case "q":
                        //cleans up after the user leaves the chat.
                        onlineUsers.remove(currentUser);
                        go = false;
                        System.out.println(currentUser + " has left the chat " +
                                "!");
                        System.out.println("waiting for connections!");
                        break;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
