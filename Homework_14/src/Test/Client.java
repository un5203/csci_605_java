package Test;

import java.io.IOException;
import java.net.*;
import java.util.Scanner;

/**
 * Created by Ujwal on 09/05/2016.
 */
public class Client {
    private DatagramSocket socket;
    private InetAddress address;
    private int port;

    public Client() throws SocketException, UnknownHostException {
        socket = new DatagramSocket();
        address = InetAddress.getByName("localhost");
        port = 12345;
    }

    public DatagramPacket getMessage() throws IOException {
        byte[] buffer = new byte[1024];
        DatagramPacket packet = new DatagramPacket(buffer,buffer.length);
        socket.receive(packet);
        return packet;
    }



    public String readMessage(DatagramPacket packet) throws IOException {
        return new String(packet.getData(),0,packet.getLength());
    }

    public void writeMessageToServer(String message)
            throws IOException {
        byte[] buffer = message.getBytes();
        DatagramPacket outpacket = new DatagramPacket(buffer,buffer.length,
                getAddress(),getPort());
        socket.send(outpacket);
    }

//    public void writeMessageToUser(String message, String username) throws
//            IOException {
//        byte[] buffer = message.getBytes();
//        DatagramPacket packet = new DatagramPacket(buffer,buffer.length,user
//                .getAddress()
//                ,user.getPort());
//        socket.send(packet);
//    }

    public static void main(String[] args) throws IOException {
        Client client = new Client();
        System.out.println("Please enter your username");
        Scanner input = new Scanner(System.in);
        String username = input.nextLine();
        client.writeMessageToServer(username);
        System.out.println(client.readMessage(client.getMessage()));
    }

    public InetAddress getAddress() {
        return address;
    }

    public int getPort() {
        return port;
    }
}
