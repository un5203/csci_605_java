package Test;

import Homework_14.v1.User;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * Created by Ujwal on 09/05/2016.
 */
public class Server {
    private DatagramSocket socket;

    public Server() throws SocketException {
        socket = new DatagramSocket(12345);
    }

    public DatagramPacket readMessage() throws IOException {
        byte[] buffer = new byte[1024];
        DatagramPacket packet = new DatagramPacket(buffer,buffer.length);
        socket.receive(packet);
        return packet;
    }

    public String readMessage(DatagramPacket packet) throws IOException {
        return new String(packet.getData(),0,packet.getLength());
    }


    public void writeMessage(String message,DatagramPacket packet) throws IOException {
        byte[] buffer = message.getBytes();
        DatagramPacket outpacket = new DatagramPacket(buffer,buffer.length,
                packet.getAddress(),packet.getPort());
        socket.send(outpacket);
    }

    public void writeMessage(String message, User user) throws IOException {
        byte[] buffer = message.getBytes();
        DatagramPacket packet = new DatagramPacket(buffer,buffer.length,user
                .getAddress()
                ,user.getPort());
        socket.send(packet);
    }

    public static void main(String[] args) throws IOException {
        Server server = new Server();
        System.out.println("waiting for connection !");
        DatagramPacket packet = server.readMessage();
        System.out.println(server.readMessage(packet));
        System.out.println("packet received!");
        server.writeMessage("hello ujwal",packet);
    }
}
