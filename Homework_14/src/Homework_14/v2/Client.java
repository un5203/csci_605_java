/**
 * Client.java
 *
 * Version 1.0.1
 */
package Homework_14.v2;


import java.io.*;
import java.net.*;
import java.util.Calendar;
import java.util.Scanner;

/**
 * Creates a client that connects to the chat server.
 *
 * @author Ujwal Bharat Nagumantri
 * @author Yeshwanth Raja
 */
public class Client implements Runnable{

    private DatagramSocket socket; //socket that connects this user to the
    // server
    private String currentUser; // name of the current user
    private DatagramPacket packet;
    private InetAddress address;
    private int port;

    /**
     * Constructor
     * @param hostname
     * @param port
     * @throws SocketException
     * @throws UnknownHostException
     */
    public Client(String hostname, int port) throws SocketException, UnknownHostException {
        byte[] buffer = new byte[1024];
        this.socket = new DatagramSocket();
        this.address = InetAddress.getByName(hostname);
        this.port = port;
        this.packet = new DatagramPacket(buffer,buffer.length,address,this
                .port);
    }

    /**
     * Sends a message to the server.
     * @param message
     * @throws IOException
     */
    public void writeMessage(String message) throws IOException {
        byte[] buffer = message.getBytes();
        DatagramPacket outpacket = new DatagramPacket(buffer,buffer.length,
                packet.getAddress(),packet.getPort());
        socket.send(outpacket);
    }

    /**
     * Reads a message from the server.
     * @return
     * @throws IOException
     */
    public String readMessage() throws IOException {
        byte[] buffer = new byte[1024];
        packet = new DatagramPacket(buffer,buffer.length);
        socket.receive(packet);
        return new String(packet.getData(),0,packet.getLength());
    }

    /**
     * Sends a message to another user.
     * @param user
     * @param message
     * @throws IOException
     */
    public void writeMessageTo(User user, String message) throws IOException {
        byte[] buffer = message.getBytes();
        DatagramPacket packet = new DatagramPacket(buffer,buffer.length,user
                .getAddress()
                ,user.getPort());
        socket.send(packet);
    }

    /**
     * Displays a message
     * @param message
     */
    public String generateMessage(String message,String fromUser){
        String temp2 =  "(" + Calendar.getInstance()
                .getTime() +
                ") - " + fromUser+ " : " +
                message + "\n";
        String temp1 = printLooper("#",temp2.length());
        String temp3 = printLooper("#",temp2.length());
        return "\n"+temp1+temp2+temp3;
    }

    /**
     * Prints a character a certain number of times
     * @param character
     * @param noOfTimes
     */
    public String printLooper(String character, int noOfTimes){
        String temp = "";
        for(int i=0;i<noOfTimes;i++){
            temp += character;
        }
        temp += "\n";
        return temp;
    }

    /**
     * Main function.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        // connects to the
        // server.
        Client client = new Client("localhost",12345); //creates a new client
        Scanner input = new Scanner(System.in); //gets the username from user
        while(true){
            //if username exists, retry.
            System.out.println("Please enter your username:");
            String username = input.nextLine();
            client.currentUser = username;
            client.writeMessage(username);
            String response = client.readMessage();
            if(response.equals("notOk")){
                System.out.println(client.readMessage());
            }
            else{
                System.out.println("connected to server !");
                break;
            }
        }
        boolean go = true;
        System.out.println("\nPlease select one of the options:");
        System.out.println("l - list of users");
        System.out.println("m - send message to another user");
        System.out.println("q - quit");
//        Thread thread = new Thread(client);
//        thread.start();
        while(go){
            //synch here
            String option = input.nextLine();
            client.writeMessage(option);
            if(option.equals("m")){
                System.out.println("Enter the name of the user to " +
                        "send a message to:");
                String toUser = input.nextLine();
                System.out.println("Enter your message");
                String toMessage = input.nextLine();
                client.writeMessage(toUser);
                toMessage = client.generateMessage(toMessage,client.currentUser);
                client.writeMessage(toMessage);
            }
            if(option.equals("q")){
                client.socket.close();
                System.out.println("Bye Bye!");
                go = false;
                System.exit(0);
            }
        }
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        while(true){
            try {
                String serverResponse = readMessage();
                System.out.println(serverResponse);
                System.out.println("\nPlease select one of the options:");
                System.out.println("l - list of users");
                System.out.println("m - send message to another user");
                System.out.println("q - quit");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
