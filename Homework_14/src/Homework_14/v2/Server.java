/**
 * Server.java
 * Version 1.0.1
 */

package Homework_14.v2;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * Starts a chat server and creates a separate thread for each server.
 *
 * @author Ujwal Bharat Nagumantri
 * @author Yeshwanth Raja
 */
public class Server{

    //List of online users.
    private Map<String,User> onlineUsers;

    /**
     * Constructor
     */
    public Server(){
        this.onlineUsers = new HashMap<>();
    }

    /**
     * Main method
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        Server server = new Server();
        DatagramSocket socket = new DatagramSocket(12345);
        byte[] buffer = new byte[1024];
        DatagramPacket packet = null;
        while(true) {
            System.out.println("waiting for connections!");
            packet = new DatagramPacket(buffer, buffer.length);
            socket.receive(packet);
            System.out.println("a client has connected!");
            //creates a new thread for the client.
            new Thread(new ChatClientHandler(socket, server.onlineUsers,packet))
                    .start();
        }
    }
}
