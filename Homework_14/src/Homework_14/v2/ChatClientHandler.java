package Homework_14.v2;
/**
 * ChatClientHandler.java
 * Version 1.01
 */

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Socket;
import java.util.Calendar;
import java.util.Map;

/**
 * Handles one unique user and enables him/her to message other users.
 *
 * @author Ujwal Bharat Nagumantri
 * @author Yeshwanth Raja
 */
public class ChatClientHandler implements Runnable{

    private DatagramSocket socket; //connection between one user and server.
    private Map<String,User> onlineUsers; //list of all online users.
    private DatagramPacket packet;
    private String currentUser;

    /**
     * Constructor
     * @param onlineUsers
     * @param packet
     */
    public ChatClientHandler(DatagramSocket socket,Map<String, User>
            onlineUsers, DatagramPacket packet) {
        this.socket = socket;
        this.onlineUsers = onlineUsers;
        this.packet = packet;
    }

    /**
     * Writes a message to connected user.
     * @param message : message to the connected user.
     * @throws IOException
     */
    public void writeMessage(String message) throws IOException {
        byte[] buffer = message.getBytes();
        DatagramPacket outpacket = new DatagramPacket(buffer,buffer.length,
                packet.getAddress(),packet.getPort());
        socket.send(outpacket);
    }

    /**
     * Forwards a message from currentUser to another user.
     * @param user : connection to another user.
     * @param message : message to another user.
     * @throws IOException
     */
    public void writeMessageTo(User user,String message) throws IOException {
        byte[] buffer = message.getBytes();
        DatagramPacket packet = new DatagramPacket(buffer,buffer.length,user
                .getAddress()
                ,user.getPort());
        socket.send(packet);
    }

    /**
     * Receives a message from the currentUser.
     * @return
     */
    public String readFirstMessage() throws IOException {
        return new String(packet.getData(),0,packet.getLength());
    }

    /**
     * Receives a message from the currentUser.
     * @return
     */
    public String readMessage() throws IOException {
        byte[] buffer = new byte[1024];
        packet = new DatagramPacket(buffer,buffer.length);
        socket.receive(packet);
        String temp = new String(packet.getData(),0,packet.getLength());
        return temp;
    }

    /**
     * Gets list of online users.
     * @return
     */
    public String getOnlineUsers(){
        String message = "";
        for(String user : onlineUsers.keySet()){
            message += "[" +user+"] ";
        }
        return message;
    }

    /**
     * Makes the user's message prettier.
     * @param message
     * @param fromUser
     * @return
     */
    public String generateMessage(String message,String fromUser){
        String temp2 =  "(" + Calendar.getInstance()
                .getTime() +
                ") - " + fromUser+ " : " +
                message + "\n";
        String temp1 = printLooper("#",temp2.length());
        return "\n"+temp1+temp2+temp1;
    }

    /**
     * Prints a character a certain number of times
     * @param character
     * @param noOfTimes
     */
    public String printLooper(String character, int noOfTimes){
        String temp = "";
        for(int i=0;i<noOfTimes;i++){
            temp += character;
        }
        temp += "\n";
        return temp;
    }

    /**
     * Creates an error message.
     * @param message
     * @return
     */
    public String generateError(String message){
        String temp1 = printLooper("!",message.length());
        return "\n"+temp1+message+"\n"+temp1;
    }


    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        try {
            while(true) {
                //First time interaction with user.
                //Here the user's login username is received and checked for
                // uniqueness.
                String username = readFirstMessage();
                if (onlineUsers.containsKey(username)) {
                    writeMessage("notOk");
                    writeMessage(generateError("User already exists!"));
                } else {
                    User user = new User(username,packet.getAddress(),packet
                            .getPort());
                    onlineUsers.put(username, user);
                    currentUser = username;
                    writeMessage("ok");
                    System.out.println(username + " has connected !");
                    break;
                }
            }
            boolean go = true;
            while(go){
                //Here input is received from user and an appropriate action
                // is performed.
                System.out.println("i'm in the second loop!");
                String option = readMessage();
                switch(option){
                    case "l":
                        //returns a list of users
                        writeMessage(getOnlineUsers());
                        break;
                    case "m":
                        //sends a message to another user.
                        String toUser = readMessage();
                        String toMessage = readMessage();
                        if(onlineUsers.containsKey(toUser)){
                            writeMessageTo(onlineUsers.get(toUser),toMessage);
                            writeMessage("\nYour message was successfully " +
                                    "sent!");
                        }
                        else{
                            writeMessage(generateError("Unable to send a message to "
                                    +toUser+
                                    "!!"));
                        }
                        break;
                    case "q":
                        //cleans up after the user leaves the chat.
                        onlineUsers.remove(currentUser);
                        go = false;
                        System.out.println(currentUser + " has left the chat " +
                                "!");
                        System.out.println("waiting for connections!");
                        break;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
