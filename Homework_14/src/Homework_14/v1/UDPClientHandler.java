/**
 * UDPClientHandler.java
 * Version 1.0.1
 */
package Homework_14.v1;


import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.Calendar;
import java.util.Map;

/**
 * Handles the individual client requests for chatting.
 *
 * @author Ujwal Bharat Nagumantri
 * @author Yeshwanth Raja
 */
public class UDPClientHandler implements Runnable{

    private DatagramSocket socket; // Server's socket
    private Map<String,User> users; //all connected users
    private String currentUser; // current user's name

    /**
     * Constructor
     * @param users
     * @param port
     * @throws SocketException
     */
    public UDPClientHandler(Map<String, User> users, int port) throws SocketException{
        socket = new DatagramSocket(port);
        this.users = users;
    }

    /**
     * Gets a datagrampacket from the connected user
     * @return
     * @throws IOException
     */
    public DatagramPacket getMessage() throws IOException {
        byte[] buffer = new byte[1024];
        DatagramPacket packet = new DatagramPacket(buffer,buffer.length);
        socket.receive(packet);
        return packet;
    }

    /**
     * Reads a message from a returned packet.
     * @param packet
     * @return
     * @throws IOException
     */
    public String readMessage(DatagramPacket packet) throws IOException {
        return new String(packet.getData(),0,packet.getLength());
    }

    /**
     * Writes a message to the server.
     * @param message
     * @param packet
     * @throws IOException
     */
    public void writeMessage(String message,DatagramPacket packet) throws IOException {
        byte[] buffer = message.getBytes();
        DatagramPacket outpacket = new DatagramPacket(buffer,buffer.length,
                packet.getAddress(),packet.getPort());
        socket.send(outpacket);
    }

    /**
     * Writes a message from current user to another user.
     * @param message
     * @param user
     * @throws IOException
     */
    public void writeMessage(String message, User user) throws IOException {
        byte[] buffer = message.getBytes();
        DatagramPacket packet = new DatagramPacket(buffer,buffer.length,user
                .getAddress()
                ,user.getPort());
        socket.send(packet);
    }

    /**
     * Returns a list of online users.
     * @return
     */
    public String getOnlineUsers(){
        String message = "";
        for(String user : users.keySet()){
            message += "[" +user+"] ";
        }
        return message;
    }

    /**
     * Generates a pretty message
     * @param message
     * @param fromUser
     * @return
     */
    public String generateMessage(String message,String fromUser){
        String temp2 =  "(" + Calendar.getInstance()
                .getTime() +
                ") - " + fromUser+ " : " +
                message + "\n";
        String temp1 = printLooper("#",temp2.length());
        return "\n"+temp1+temp2+temp1;
    }

    /**
     * Prints a character a certain number of times
     * @param character
     * @param noOfTimes
     */
    public String printLooper(String character, int noOfTimes){
        String temp = "";
        for(int i=0;i<noOfTimes;i++){
            temp += character;
        }
        temp += "\n";
        return temp;
    }

    /**
     * generates an error message
     * @param message
     * @return
     */
    public String generateError(String message){
        String temp1 = printLooper("!",message.length());
        return "\n"+temp1+message+"\n"+temp1;
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        boolean go = true;
        try {
            DatagramPacket packet = getMessage();
            String username = readMessage(packet);
            System.out.println(username + " is trying to connect!");
            if(users.containsKey(username)){
                writeMessage("error",packet);
                writeMessage("User already exists",packet);
            }
            else{
                currentUser = username;
                User user = new User(username,packet.getAddress(),packet.getPort());
                users.put(username,user);
                System.out.println(username + " has connected!");
                writeMessage("ok",packet);
                while(go){
                    packet = getMessage();
                    String option = readMessage(packet);
                    switch(option){
                        case "l":
                            writeMessage(getOnlineUsers(),packet);
                            break;
                        case "m":
                            //send a message to users
                            break;
                        case "q":
                            users.remove(username);
                            currentUser = "";
                            go = false;
                            System.out.println(username + " has left the " +
                                    "chat!");
                            break;
                        case "messagetosomeone":
                            String toUser = readMessage(getMessage());
                            System.out.println(toUser);
                            String toMessage = readMessage(getMessage());
                            if(users.containsKey(toUser) && !toUser.equals
                                    (currentUser)){
                                //send the message
                                writeMessage("Your message was successfully " +
                                        "sent !",packet);
                                writeMessage(toMessage,users.get(toUser));
                            }
                            else if(toUser.equals(currentUser)){
                                writeMessage(generateError("You cannot send " +
                                        "a message to yourself"),packet);
                            }
                            else{
                                writeMessage(generateError("User " + toUser+"" +
                                        " " +
                                        "is not online" +
                                        "!"),packet);
                            }
                        default:
                            break;
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
