/**
 * UDPClient.java
 * Version 1.0.1
 */
package Homework_14.v1;

import java.io.*;
import java.net.*;
import java.util.Calendar;
import java.util.Scanner;

/**
 * Creates a client that connects to the server using UDP
 *
 * @author Ujwal Bharat Nagumantri
 * @author Yeshwanth Raja
 */
public class UDPClient implements Runnable {

    private DatagramSocket socket; //server's socket
    private InetAddress address; // address of the server
    private int port; // server's port

    /**
     * Constructor
     * @param hostname
     * @param port
     * @throws SocketException
     * @throws UnknownHostException
     */
    public UDPClient(String hostname, int port) throws
            SocketException, UnknownHostException {
        socket = new DatagramSocket();
        address = InetAddress.getByName(hostname);
        this.port = port;
    }

    /**
     * Server socket getter.
     * @return
     */
    public DatagramSocket getSocket(){
        return socket;
    }

    /**
     * Server port getter
     * @return
     */
    public int getPort(){
        return port;
    }

    /**
     * Server's inetadress getter.
     * @return
     */
    public InetAddress getAddress() {
        return address;
    }
    /**
     * Gets a datagrampacket from server.
     * @return
     * @throws IOException
     */
    public DatagramPacket getMessage() throws IOException {
        byte[] buffer = new byte[1024];
        DatagramPacket packet = new DatagramPacket(buffer,buffer.length);
        getSocket().receive(packet);
        return packet;
    }

    /**
     * Reads a message from a returned packet.
     * @param packet
     * @return
     * @throws IOException
     */
    public String readMessage(DatagramPacket packet) throws IOException {
        return new String(packet.getData(),0,packet.getLength());
    }

    /**
     * Writes a message to server.
     * @param message
     * @throws IOException
     */
    public void writeMessageToServer(String message)
            throws IOException {
        byte[] buffer = message.getBytes();
        DatagramPacket outpacket = new DatagramPacket(buffer,buffer.length,
                getAddress(),getPort());
        socket.send(outpacket);
    }

    /**
     * Displays a message
     * @param message
     */
    public String generateMessage(String message,String fromUser){
        String temp2 =  "(" + Calendar.getInstance()
                .getTime() +
                ") - " + fromUser+ " : " +
                message + "\n";
        String temp1 = printLooper("#",temp2.length());
        String temp3 = printLooper("#",temp2.length());
        return "\n"+temp1+temp2+temp3;
    }

    /**
     * Prints a character a certain number of times
     * @param character
     * @param noOfTimes
     */
    public String printLooper(String character, int noOfTimes){
        String temp = "";
        for(int i=0;i<noOfTimes;i++){
            temp += character;
        }
        temp += "\n";
        return temp;
    }


    /**
     * Main function
     * @param args
     * @throws IOException
     * @throws InterruptedException
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        boolean go = true;
        UDPClient client = new UDPClient("localhost",12345);
        System.out.println("connecting to server");
        client.writeMessageToServer("initiateHandshake");
        int port = Integer.parseInt(client.readMessage(client.getMessage()));
        client = new UDPClient("localhost",port);
        System.out.println("connection established!");
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter your username");
        String username = input.nextLine();
        client.writeMessageToServer(username);
        DatagramPacket packet = client.getMessage();
        String message = client.readMessage(packet);
        if(message.equals("error")){
            System.out.println(client.readMessage(client.getMessage()));
        }
        if(message.equals("ok")){
            //create thread here
            //read incoming message
            System.out.println("\nPlease select one of the options:");
            System.out.println("l - list of users");
            System.out.println("m - send message to another user");
            System.out.println("q - quit");
            while(go){
                //synch here
                String option = input.nextLine();
                client.writeMessageToServer(option);
                if(option.equals("m")){
                    System.out.println("Enter the name of the user to " +
                            "send a message to:");
                    String toUser = input.nextLine();
                    System.out.println("Enter your message");
                    String toMessage = input.nextLine();
                    client.writeMessageToServer("messagetosomeone");
                    client.writeMessageToServer(toUser);
                    toMessage = client.generateMessage(toMessage,username);
                    client.writeMessageToServer(toMessage);
                }
                if(option.equals("q")){
                    System.out.println("Bye Bye!");
                    go = false;
                    System.exit(0);
                }
                Thread thread = new Thread(client);
                thread.start();
            }
        }
    }



    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        while(true){
            try {
                String serverResponse = readMessage(getMessage());
                System.out.println(serverResponse);
                System.out.println("\nPlease select one of the options:");
                System.out.println("l - list of users");
                System.out.println("m - send message to another user");
                System.out.println("q - quit");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

