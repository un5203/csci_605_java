/**
 * UDPServer.java
 * Version 1.0.1
 */
package Homework_14.v1;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.HashMap;
import java.util.Map;

/**
 * Creates a server and separate threads for each connected client.
 *
 * @author Ujwal Bharat Nagumantri
 * @author Yeshwanth Raja
 */
public class UDPServer {

    private Map<String,User> users; //List of connected users

    /**
     * Constructor
     */
    public UDPServer(){
        users = new HashMap<>();
    }

    /**
     * Main function
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        UDPServer server = new UDPServer();
        DatagramSocket socket = new DatagramSocket(12345);
        int port = 12346;
        while(true){
            byte[] buffer = new byte[1024];
            DatagramPacket packet = new DatagramPacket(buffer,buffer.length);
            System.out.println("waiting for connections");
            socket.receive(packet);
            System.out.println("a client connected!");
            buffer = (String.valueOf(port)).getBytes();
            packet = new DatagramPacket(buffer,buffer.length,packet
                    .getAddress(),packet.getPort());
            new Thread(new UDPClientHandler(server.users,port)).start();
            System.out.println("moving him to a new port!");
            socket.send(packet);
            port += 1;
        }
    }
}
