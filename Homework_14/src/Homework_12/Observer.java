package Homework_12; /**
 * Observer.java
 * Version 1.0.1
 */

/**
 * Interface for implementing the observer pattern.
 */
public interface Observer {
    public void update(String message, String flag);
    public void youGotMail();
}
