package Homework_12; /**
 * ChatException.java
 * Version 1.0.1
 */

/**
 * Custom Exceptions are thrown using this class.
 */
public class ChatException extends Exception {
    public ChatException(String message){
        super(message);
    }
}
