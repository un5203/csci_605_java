package Homework_12; /**
 * ChatModel.java
 * Version 1.0.1
 */

import java.io.*;
import java.util.Calendar;

/**
 * Implements the application logic that acts as the backend for messages.
 *
 * @author Ujwal Bharat
 * @author Yeshwanth Raja
 */
public class ChatModel implements Runnable{

    private String currentUser = ""; //name of the currentuser
    private File messagesDir; //root directory of messages
    private File currentUserFile; //current user's messages are stored here
    private Observer observer; //observer pattern
    private long timestamp; //keep track of file modifications

    /**
     * Constructor
     */
    public ChatModel(){
        messagesDir = new File("messages");
        messagesDir.mkdir();
    }

    /**
     * Keeps track of observers.
     * @param observer
     */
    public void attachObserver(Observer observer){
        this.observer = observer;
    }

    /**
     * Notifies the observer with a generic or error message
     * @param message - message to pass
     * @param flag - "error" | "general" (type of message)
     */
    public void notifyObserver(String message,String flag){
        observer.update(message,flag);
    }

    /**
     * Notify observers that messages have been sent to them.
     */
    public void checkMessages(){
        observer.youGotMail();
    }

    /**
     * When a new user joins, his/her name is converted to a file and added
     * here.
     */
    public void addUser(String username) throws IOException, ChatException {
        if(isUserOnline(username)){
            throw new ChatException("User already exists");
        }
        File userFile = new File("messages/" + username);
        userFile.createNewFile();
        currentUserFile = userFile;
        currentUser = username;
        timestamp = currentUserFile.lastModified();
        new Thread(this).start();
    }

    /**
     * Checks whether a user is online or not
     * @param username
     * @return True if user is online
     */
    public boolean isUserOnline(String username){
        for(File user : messagesDir.listFiles()){
            if(user.getName().equals(username)){
                return true;
            }
        }
        return false;
    }

    /**
     * Display all the online users
     * @return
     */
    public String showOnlineUsers(){
        String temp = "";
        for(File user : messagesDir.listFiles()){
            temp += "[" + user.getName() + "]";
        }
        return temp;
    }

    /**
     * sends a message to a user
     * @param message
     * @param toUsername
     * @throws IOException
     */
    public void sendMessageTo(String message, String toUsername) throws ChatException, IOException {
        if(toUsername.equals(currentUser)){
            throw new ChatException("Cannot send a message to yourself!");
        }
        if(isUserOnline(toUsername)){
            File temp = new File("messages/" + toUsername);
            BufferedWriter writer = new BufferedWriter(new FileWriter(temp,
                    true));
            String toWrite =  "(" + Calendar.getInstance()
                    .getTime() +
                    ") - " + currentUser+ " : " +
                    message + "\n";
            writer.write(toWrite);
            writer.close();
        }
    }

    /**
     * Removes the user files and quits
     */
    public void quit(){
        if(isUserOnline(currentUser)){
            currentUserFile.delete();
            currentUser = "";
        }
    }

    /**
     * Returns messages of a particular user
     * @return
     * @throws IOException
     */
    public String getMessages() throws IOException {
        if(isUserOnline(currentUser)){
            File temp = new File("messages/"+currentUser);
            BufferedReader reader = new BufferedReader(new FileReader(temp));
            String message = "";
            int n;
            while( (n = reader.read())!= -1){
                message += (char)n + reader.readLine();
                message += "\n";
            }
            BufferedWriter writer = new BufferedWriter(new FileWriter(temp));
            writer.write("");
            writer.close();
            reader.close();
            return message;
        }
        return "";
    }

    /**
     * main function to test the model
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        ChatModel model = new ChatModel();
        model.addUser("ujwal");
        model.addUser("john");
        model.sendMessageTo("Hey there!","ujwal");
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        while(true){
            if(currentUserFile.exists() && timestamp != currentUserFile.lastModified()){
                observer.youGotMail();
//                checkMessages();
                timestamp = currentUserFile.lastModified();
            }
            else{
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
