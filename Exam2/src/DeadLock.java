public class DeadLock implements Runnable{
    private Object lock1;
    private Object lock2;

    public DeadLock(Object lock1, Object lock2){
        this.lock1 = lock1;
        this.lock2 = lock2;
    }

    public static void main(String[] args) {
        Object a = new Object();
        Object b = new Object();
        new Thread(new DeadLock(a,b)).start();
        new Thread(new DeadLock(b,a)).start();
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p/>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        while(true){
            synchronized (lock1){
                synchronized (lock2){
                    System.out.println("hello");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
