/**
 * Created by Ujwal on 19/04/2016.
 */
public class Calculator{

    public double divide(double a, double b) throws DivideByZeroException{
        if (b==0){
            throw new DivideByZeroException();
        }
        return a/b;
    }
}
