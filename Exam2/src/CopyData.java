import java.io.*;

public class CopyData {
        public static void main(String[] args) throws FileNotFoundException {
            try{
                InputStream in = new FileInputStream("input.bin");
                OutputStream out = new FileOutputStream("output.bin");
                int n;
                byte[] buf = new byte[1024];
                while((n = in.read(buf)) != -1){
                    out.write(buf,0,n);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally{
//                in.close();
//                out.close();
            }
        }
    }

