public class Sleeper implements Runnable {
    public void run() {
        try {
            System.out.println("Taking a nap.");
            Thread.sleep(100000);
            System.out.println("Slept peacefully.");
        }
        catch(InterruptedException e) {
            System.out.println("RUDELY INTERRUPTED!!");
        }
    }
    public static void main(String[] args) throws InterruptedException {
        Sleeper sleeper = new Sleeper();
        Thread thread = new Thread(sleeper);
        thread.start();
        Thread.sleep(1000);
        thread.interrupt();
    }
}