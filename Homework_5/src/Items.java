/*
 * Items.java
 *
 *  Version 1.01
 */

/**
 *
 * This is an abstract class for items
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */

public abstract class Items<T,V>{

    //add one pair of left and right items to the collection
    public abstract void add(T t, V v);

    //adds all items in an array to the items collection
    public void addAll(T[] leftItems, V[] rightItems){
        for(int i = 0;i<leftItems.length;i++){
            add(leftItems[i],rightItems[i]);
        }
    };

    //returns the number of item pairs stored in the collection
    public abstract int size();

    //returns the item from the left of the collection at a particular index
    public abstract T getLeft(int index);

    //returns the item from the right of the collection at a particular index
    public abstract V getRight(int index);

    //returns the item pair from right and left of the collection at a particular index
    public abstract Items get(int index);


    //prints the contents of the items array
    public String toString(){
        String output = "";
        for(int i = 0;i< size();i++){
            output += "\""+getLeft(i)+"\" : "+ "\""+ getRight(i)+"\"\n ";
        }
        return output;
    }

}