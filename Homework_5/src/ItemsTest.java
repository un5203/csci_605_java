/*
 * ItemsTest.java
 *
 *  Version 1.01
 */

/**
 *
 * Tests the different implementations of the items collection
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */


public class ItemsTest {
    //function that accepts an array of items and tests its limits
    public static void testItems(Items items){
        Integer[] intArray = {0,1,2,3,4,5};
        String[] strArray = {"alpha","bravo","charlie","delta","echo","foxtrot"};
        Float[] fltArray = {2.0f,2.1f,2.2f,2.3f,2.4f,2.5f};
        Double[] dblArray = {3.0,3.1,3.2,3.3,3.4,3.5};
        items.addAll(intArray,strArray);
        System.out.println("========== Original List ==========="+"\n"+ items.toString());
        items.add(6,"georgia");
        System.out.println("========== After Adding new item ==========="+"\n"+items.toString());
        System.out.println("Size is "+" "+items.size());
        System.out.println("Getting Item at 3 position --"+" "+items.get(3));
        System.out.println("");
    }

    //main function that tests the ArrayOfItems and ListOfItems functionality
    public static void main(String[] args) {
        ItemsTest test = new ItemsTest();
        ListOfItems list = new ListOfItems();
        System.out.println("###############################");
        System.out.println("Using list");
        test.testItems(list);
        System.out.println("###############################");
        ArrayOfItems array = new ArrayOfItems();
        System.out.println("Using Array");
        test.testItems(array);
    }
}