/*
 * ListOfItems.java
 *
 *  Version 1.01
 */

/**
 * This class extends items and creates a linkedlist type
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */

public class ListOfItems<T, V> extends Items<T, V> {


    private Node head1;
    private Node tail1;
    private Node head2;
    private Node tail2;
    private int size = 0;
    private boolean firstTime = true; //checks if the add method is called for the first time or not

    public ListOfItems() {

        Node node1 = new Node();
        head1 = node1;
        tail1 = node1;

        Node node2 = new Node();
        head2 = node2;
        tail2 = node2;
    }

    //function that adds a pair of items to the array
    @Override
    public void add(T t, V v) {
        if (firstTime == true) {
            head1.setItem(t);
            head2.setItem(v);
            size += 1;
            firstTime = false;
        } else {
            Node node_left = new Node();
            node_left.setItem(t);
            tail1.setNext(node_left);
            tail1 = node_left;

            Node node_right = new Node();
            node_right.setItem(v);
            tail2.setNext(node_right);
            tail2 = node_right;
            size += 1;
        }
    }

    //function that returns a pair of items from the left and right array using an index
    @Override
    public Items get(int index) {

        Items<T,V> item = new ArrayOfItems<T, V>();
        item.add(getLeft(index),getRight(index));
        return item;

    }

    //function that returns the size of the array
    @Override
    public int size() {
        return this.size;
    }

    //function that returns an item from the left array using an index
    @Override
    public T getLeft(int index) {
        int count = 0;
        Node temp = head1;
        while (count < index) {
            if (temp.getNext() != null) {
                temp = temp.getNext();
                count++;
            } else {
                break;
            }
        }
        return (T) temp.getItem();
    }

    //function that returns an item from the right array using an index
    @Override
    public V getRight(int index) {
        int count = 0;
        Node temp = head2;
        while (count < index) {
            if (temp.getNext() != null) {
                temp = temp.getNext();
                count++;
            } else {
                break;
            }
        }
        return (V) temp.getItem();
    }


}
