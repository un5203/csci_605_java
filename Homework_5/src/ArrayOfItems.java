/*
 * ArrayOfItems.java
 *
 *  Version 1.01
 */

/**
 *
 * This class extends items
 *
 * @ author  Ujwal Bharat
 * @ author Yeshwanth Raja
 */

public class ArrayOfItems<T,V> extends Items<T,V>{

    //variables that hold the items
    private T[] leftItemList;
    private V[] rightItemList;
    private T[] leftTempHolder;
    private V[] rightTempHolder;

    //initial constructor
    public ArrayOfItems() {
        leftItemList = (T[]) new Object[5];
        leftTempHolder = (T[]) new Object[5];
        rightItemList = (V[]) new Object[5];
        rightTempHolder = (V[]) new Object[5];
    }

    //function that adds a pair of items to the array
    @Override
    public void add(T t, V v) {
        if(size()>leftItemList.length-5){
            for(int i = 0;i<leftItemList.length;i++){
                leftTempHolder[i]= leftItemList[i];
                rightTempHolder[i]= rightItemList[i];
            }
            leftItemList = (T[]) new Object[leftTempHolder.length+30];
            rightItemList = (V[]) new Object[rightTempHolder.length+30];

            for(int i = 0;i<leftTempHolder.length;i++){
                leftItemList[i] = leftTempHolder[i];
                rightItemList[i] = rightTempHolder[i];
            }
        }
        for(int i = 0;i<leftItemList.length;i++){
            if(getLeft(i) == null)
            {
                leftItemList[i] = t;
                rightItemList[i] = v;
                break;
            }
        }
    }

    //function that returns the size of the array
    @Override
    public int size() {
        if(leftItemList.length == rightItemList.length){
            int count = 0;
            for(int i=0;i<leftItemList.length;i++){
                if(getLeft(i)!= null){
                    count++;
                }
            }
            return count;
        }
        else{
            System.out.println("Item pair mismatch !");
            return 0;
        }
    }

    //function that returns an item from the left array using an index
    @Override
    public T getLeft(int index) {
        return leftItemList[index];
    }

    //function that returns an item from the right array using an index
    @Override
    public V getRight(int index) {
        return rightItemList[index];
    }

    //function that returns a pair of items from the left and right array using an index
    @Override
    public Items get(int index) {
        Items<T,V> item = new ArrayOfItems<T, V>();
        item.add(leftItemList[index],rightItemList[index]);
        return item;
    }

    //main function to test this implementation
    public static void main(String[] args) {
        Integer[] intArray = {0,1,2,3,4,5};
        String[] strArray = {"a","b","c","d","e","f"};
        Float[] fltArray = {2.0f,2.1f,2.2f,2.3f,2.4f,2.5f};
        Double[] dblArray = {3.0,3.1,3.2,3.3,3.4,3.5};
        Items<Integer,String> item1 = new ArrayOfItems<>();
        item1.add(0,"a");
        item1.add(1,"b");
        item1.add(2,"c");
        item1.add(3,"d");
        item1.add(4,"e");
        item1.add(5,"f");
        item1.add(6,"g");
        item1.add(7,"h");
        item1.add(8,"i");
        item1.add(9,"j");
        Items<Integer,String> item2 = new ArrayOfItems<>();
        item2.addAll(intArray,strArray);
        System.out.println(item2.toString());
        System.out.println(item2.get(3));
        Items<Float,Double> item3 = new ArrayOfItems<>();
        item3.addAll(fltArray,dblArray);
        System.out.println(item3.toString());
        System.out.println(item3.get(2));
    }
}